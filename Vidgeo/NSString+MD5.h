//
//  NSString+MD5.h
//  Vidgeo
//
//  Created by John Mercouris on 3/24/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "NSString+MD5.h"

@interface NSString(MD5)

- (NSString *)MD5;

@end