//
//  Constant.h
//  Vidgeo
//
//  Created by John Mercouris on 3/3/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

extern int const kDailyVidgeoReward;
extern int const kShareVidgeoReward;
extern int const kUnlockTime;
extern double const kVersion;

@end
