//
//  DrivingDirections.h
//  Vidgeo
//
//  Created by John Mercouris on 3/6/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface DrivingDirections : NSObject
+(void)openDirectionsWithLatitude: (float) latitude longitude: (float) longitude;

@end
