//
//  UserVidgeoViewController.m
//  Vidgeo
//
//  Created by John Mercouris on 2/24/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "UserVidgeoViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "NSString+MD5.h"

@implementation UserVidgeoViewController
UIPopoverController * popControl;
MPMoviePlayerViewController* movieViewController;
PFObject * vidgeoData;
UITextField * alertTextField;

@synthesize tabBar;
@synthesize eyeImage, dateLabel, viewsLabel, fromVC, viewCount, downVotes, upVotes, overLay;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [tabBar setDelegate:self];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //start playing the video:
    [self.view bringSubviewToFront:self.videoView];
    [movieViewController.moviePlayer prepareToPlay];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(handleSingleTap:)];
    
    UIView *aView = [[UIView alloc] initWithFrame:movieViewController.moviePlayer.view.bounds];
    [aView addGestureRecognizer:tapGesture];
    [movieViewController.moviePlayer.view addSubview:aView];
    [movieViewController.moviePlayer play];
    
    
    
    // Bring Hidden Subviews to Top
    [self.view bringSubviewToFront:eyeImage];
    [self.view bringSubviewToFront:dateLabel];
    [self.view bringSubviewToFront:viewsLabel];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    movieViewController =[[MPMoviePlayerViewController alloc] initWithContentURL:self.videoURL];
    movieViewController.moviePlayer.controlStyle = MPMovieControlStyleNone;
    [self.videoView addSubview:movieViewController.view];
    [movieViewController.view setFrame:self.videoView.bounds];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doneButtonClick:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    self.dateLabel.text = [ParseStore stringFromDate:self.vidgeoObject.createdAt ofType:@"dd-MMM-yyyy"];
    
    //getting the number of ratings
    [self displayDownRating:0];
    [self displayUpRating:0];
    if ([self.vidgeoObject[kParseViewCount] isEqual:[NSNull null]]) {
        self.vidgeoObject[kParseViewCount] = @0;
    }
    [self displayViewCount:[self.vidgeoObject[kParseViewCount] intValue]];
}


-(void)doneButtonClick:(NSNotification*)aNotification {
    [self.view bringSubviewToFront:self.overLay];
    [self.overLay setHidden:NO];
}

#pragma mark - tabBar controls
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    switch([item tag])
    {
        case kVideoDelete:
            [self deleteTab];
            break;
        case kVideoBackButton:
            [self backTab];
            break;
        case kVideoLockButton:
            [self lockTab];
            break;
        case kVideoShareButton:
            [self shareTab];
            break;
            [self.tabBar setSelectedItem:nil];
    }
}

-(void) dismissVC {
    if ([self.fromVC isEqualToString:@"Record"]) {
        [self performSegueWithIdentifier:@"backfromRecord" sender:self];
    }else{
        [self performSegueWithIdentifier:@"unwindToMyVidgeo" sender:self];
    }
}

-(void) backTab {
    // Halt movie playback
    [movieViewController.moviePlayer stop];
    [self dismissVC];
    
}

-(void) deleteTab {
    [[[UIAlertView alloc] initWithTitle:@"Delete"
                                message:@"are you sure you want to delete this video?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Delete", nil] show];
    
}

-(void) shareTab {
    [[[UIAlertView alloc] initWithTitle:@"Share"
                                message:@""
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Share on Facebook",@"Share with select Friends",@"Share location", nil] show];
}

-(void) lockTab {
    UIAlertView * alert;
    if (self.vidgeoObject[kVidgeoLock]) {
        alert = [[UIAlertView alloc] initWithTitle:@"Vidgeo Lock" message:@"Change your secret code or unlock your vidgeo" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Unlock",@"Set",nil];
    }else{
        alert = [[UIAlertView alloc] initWithTitle:@"Vidgeo Lock" message:@"Enter a secret code to lock your video:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Set",nil];
    }
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertTextField = [alert textFieldAtIndex:0];
    alertTextField.secureTextEntry = YES;
    alertTextField.keyboardType = UIKeyboardTypeAlphabet;
    alertTextField.placeholder = @"Enter a phrase:";
    [alert show];
}

-(void)actuallyDeleteVidgeo {
    // :( deleting a beloved vidgeo
    [ParseStore deleteMyVidgeo:self.vidgeoObject];
    [movieViewController.moviePlayer stop];
    [self dismissVC];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Yes, Flag this Vidgeo"])
    {
        [ParseStore flagVidgeo:self.vidgeoObject];
        NSLog(@"flagged");
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Video Flagged", nil) message:NSLocalizedString(@"Video has been flagged for removal. (Please refresh map using \"user location button\")", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
    else if([title isEqualToString:@"Share on Facebook"]) {
        
        [self postOnWall:self.vidgeoObject];
    }
    else if([title isEqualToString:@"Share location"]) {
        [self shareLocation:self.vidgeoObject];
    }
    else if([title isEqualToString:@"Share with select Friends"]) {
        [self sendRequest:self.vidgeoObject];
    }else if([title isEqualToString:@"Set"]){
        //extract the password and update the vigdeo
        [ParseStore lockVidgeo:self.vidgeoObject withPasscode:alertTextField.text];
    }else if([title isEqualToString:@"Unlock"]){
        //extract the password and update the vigdeo
        [PFCloud callFunctionInBackground:@"unlockUserVidgeo"
                           withParameters:@{@"vidgeo": self.vidgeoObject.objectId}
                                    block:^(id success, NSError *error) {
                                        if (error) {
                                            NSLog(@"Unable to unlock the Vidgeo : %@",error);
                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error unlocking your Vidgeo" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                            [alertView show];
                                            return;
                                        }else if(success){
                                            self.vidgeoObject[kVidgeoLock]=@"";
                                            //                                            NSLog(@"Successfully ulocked the Vidgeo!!");
                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Your Vidgeo has been unlocked" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                            [alertView show];
                                        }else{
                                            NSLog(@"Failed to unlock vidgeo!");
                                        }
                                    }];
    }else if([title isEqualToString:@"Delete"]) {
        [self actuallyDeleteVidgeo];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)swipeLeft:(id)sender
{
    [self backTab];
}

- (IBAction)swipeUp:(id)sender
{
    [self backTab];
}

- (IBAction)videoCreatorInfo:(id)sender {
    //code to display the creators name
    //possibly option to follow them in the future
    
}
- (IBAction)playButton:(id)sender {
    [self handleScreenTouch];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self handleScreenTouch];
}

- (void)handleScreenTouch {
    float rate = [movieViewController.moviePlayer currentPlaybackRate];
    if (rate ==0.0f) {
        [self.overLay setHidden:YES];
        [self.view sendSubviewToBack:self.overLay];
        [movieViewController.moviePlayer play];
    }else{
        [movieViewController.moviePlayer pause];
        [self.view bringSubviewToFront:self.overLay];
        [self.overLay setHidden:NO];
    }
}

#pragma mark - overlay buttons for more info buttons
- (IBAction)listUpVotes:(id)sender {
}

- (IBAction)listDownVotes:(id)sender {
}

-(void)displayUpRating:(int)newValue {
    int rateUp = (self.vidgeoObject[kParseRateUp]) ? (int)[(NSArray*)self.vidgeoObject[kParseRateUp] count] : 0;
    rateUp +=newValue;
    NSString *stringRating = [NSString stringWithFormat:@"%d",rateUp];
    self.upVotes.text = stringRating;
    if ([self.vidgeoObject[kParseRateUp] containsObject:[PFUser currentUser].objectId]) {
        //indicate if the user has liked thier own vidgeo
    }
}

-(void)displayDownRating:(int)newValue {
    int rateDown = (self.vidgeoObject[kParseRateDown]) ? (int)[(NSArray*)self.vidgeoObject[kParseRateDown] count] : 0;
    rateDown +=newValue;
    NSString *stringRating = [NSString stringWithFormat:@"%d",rateDown];
    self.downVotes.text = stringRating;
    if ([self.vidgeoObject[kParseRateDown] containsObject:[PFUser currentUser].objectId]) {
        //indicate if the user has liked thier own vidgeo
    }
}

-(void)displayViewCount:(int)count {
    //write code here to update the viewcount UI
    self.viewCount.text = [NSString stringWithFormat:@"%i",count];
}

@end
