//
//  PieChart.h
//  Vidgeo
//
//  Created by John Mercouris on 10/28/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PieChart : NSObject

- (UIImage*)getPieImage:(float) completion;
- (UIImage*)getCenteredImageWithRating:(float)rating available: (bool) available;
- (UIImage*)getCenteredImageWithPopularity: (float) popularity available:(bool)available;
- (UIImage*)getCenteredImageWithRating:(float)rating popularity:(float)popularity available:(bool)available;

@end
