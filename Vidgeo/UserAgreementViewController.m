//
//  UserAgreementViewController.m
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 12/5/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import "UserAgreementViewController.h"

@interface UserAgreementViewController ()
@end

@implementation UserAgreementViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)agreeButton:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"USERAGREEMENT"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"USERAGREEMENT"];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end