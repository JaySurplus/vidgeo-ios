//
//  ViewController.h
//  Vidgeo
//
//  Created by John Mercouris on 10/26/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <MapKit/MapKit.h>
#import "ParseStore.h"
#import "PieChart.h"
#import "MyLogInViewController.h"
#import "MySignUpViewController.h"


@interface MapViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, MKMapViewDelegate,
                                              PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UIActionSheetDelegate,
                                              UIAlertViewDelegate, CLLocationManagerDelegate>
{
}

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, atomic) NSData *videoData;
@property (strong, nonatomic) NSMutableArray *vidgeoList;
@property (weak, nonatomic) IBOutlet MKMapView *globalMapView;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (copy,   nonatomic) NSURL *movieURL;
@property (strong, nonatomic) MPMoviePlayerController *movieController;
@property (nonatomic) CLLocationManager *locationManager;
@property  (strong, nonatomic) NSMutableDictionary *locationMapping;
- (IBAction)userFilter:(id)sender;
- (IBAction)takeVideo:(id)sender;
- (IBAction)userMapFocus:(id)sender;
- (void)dropPins:(NSArray *)cllArray;
- (IBAction)longPress:(id)sender;

@end