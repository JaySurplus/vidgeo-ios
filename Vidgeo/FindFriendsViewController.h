//
//  FindFriendsViewController.h
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 2/23/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyLogInViewController.h"
#import "MySignUpViewController.h"

@interface FindFriendsViewController : UITableViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end