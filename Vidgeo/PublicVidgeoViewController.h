//
//  VidgeoVC.h
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 10/31/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//
#import <Parse/Parse.h>
#import <MediaPlayer/MediaPlayer.h>
#import "MyLogInViewController.h"
#import "MySignUpViewController.h"
#import "SharingViewController.h"

static int const kVideoBackButton = 0;
static int const kVideoShareButton = 1;
static int const kVideoFlagButton = 2;
static int const kVideoVoteUpButton = 3;
static int const kVideoVoteDownButton = 4;

@interface PublicVidgeoViewController : SharingViewController <UITabBarDelegate, CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong, nonatomic) PFObject *vidgeoObject;
@property (strong, nonatomic) NSData *vidgeoData;
@property (strong, nonatomic) NSURL *videoURL;
@property (strong, nonatomic) IBOutlet UILabel *upVotes;
@property (strong, nonatomic) IBOutlet UILabel *downVotes;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIView *overlayInfo;
@property (strong, nonatomic) IBOutlet UILabel *viewCount;
@property (strong, nonatomic) IBOutlet UIButton *uploader;

- (IBAction)swipeLeft:(id)sender;
- (IBAction)swipeUp:(id)sender;
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer;

@end