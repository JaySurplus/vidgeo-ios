//
//  FavoritesViewController.m
//  Vidgeo
//
//  Created by John Mercouris on 2/24/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "FavoritesViewController.h"
#import "PublicVidgeoViewController.h"
#import "vidgeoCell.h"
#import "NSString+MD5.h"

@implementation FavoritesViewController
@synthesize favoritesCollectionView, activityIndicator;
@synthesize movieURL;
NSArray* tempVidgeos;
PFObject * vidgeoObject;
UITextField * alertTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.vidgeos = [[NSArray alloc] init];
    [self.activityIndicator startAnimating];
    [PFCloud callFunctionInBackground:@"thumbnailForUserFavorites"
                       withParameters:@{}
                                block:^(NSArray *myvidgeos, NSError *error) {
                                    if (error) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [self.activityIndicator stopAnimating];
                                        });
                                        NSLog(@"Couldn't display my vidgeos!");
                                        NSLog(@"%@", error);
                                    }else if(myvidgeos){
                                        tempVidgeos = myvidgeos;
                                        [self setUpImages:tempVidgeos];
                                    }else{
                                        NSLog(@"Failed to fetch the vidgeo locations!");
                                    }
                                }];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [favoritesCollectionView setDataSource:self];
    [favoritesCollectionView setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpImages:(NSArray *)images
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        // Iterate over all images and get the data from the PFFile
        for (int i = 0; i < images.count; i++)
        {
            PFObject *eachObject = [images objectAtIndex:i];
            PFFile *theImage = [eachObject objectForKey:kParseThumbnail];
            NSData *imageData = [theImage getData];
            if (imageData && ![imageData isEqual:[NSNull null]] && imageData!=nil) {
                [tempVidgeos[i] setObject:imageData forKey:kVidgeoThumbnail];
            }
        }
        self.vidgeos = tempVidgeos;
        // Update UI on main thread.
        dispatch_async(dispatch_get_main_queue(), ^{
            [favoritesCollectionView reloadData];
            [self.activityIndicator stopAnimating];
        });
    });
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.vidgeos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    vidgeoCell *cell = (vidgeoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.vidgeoThumbnail.image=nil;
    [cell.lockIcon setHidden:YES];
    if (self.vidgeos[indexPath.row][kVidgeoLock]) {
        [cell.vidgeoThumbnail setImage:[UIImage imageNamed:@"iTunesArtwork.png"]];
        [cell.lockIcon setHidden:NO];
    }else if (self.vidgeos[indexPath.row][kVidgeoThumbnail]) {
        UIImage *image = [UIImage imageWithData:self.vidgeos[indexPath.row][kVidgeoThumbnail]];
        UIImage * rotatedImage = [[UIImage alloc] initWithCGImage: image.CGImage
                                                            scale: 1.0
                                                      orientation: UIImageOrientationRight];
        [cell.vidgeoThumbnail setImage:rotatedImage];
    }else{
        [cell.vidgeoThumbnail setImage:[UIImage imageNamed:@"iTunesArtwork.png"]];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // When picture is touched, open a viewcontroller with the image
    vidgeoObject = self.vidgeos[indexPath.row];
    if (vidgeoObject[kVidgeoLock]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Unlock this Vidgeo" message:@"This Vidgeo is locked, enter the secret code to unlock it:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Unlock",nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        alertTextField = [alert textFieldAtIndex:0];
        alertTextField.secureTextEntry = YES;
        alertTextField.keyboardType = UIKeyboardTypeAlphabet;
        alertTextField.placeholder = @"secret code here:";
        [alert show];
    }else{
        [self.activityIndicator startAnimating];
        [PFCloud callFunctionInBackground:@"videoForVidgeo"
                           withParameters:@{@"vidgeo": vidgeoObject.objectId}
                                    block:^(PFObject *videoObject, NSError *error) {
                                        if (error) {
                                            [self displayError:error];
                                            return;
                                        }else if(videoObject){
                                            [self playVidgeo:videoObject];
                                            return;
                                        }else{
                                            [self displayError:error];
                                            return ;
                                        }
                                    }];
    }
}

-(void)displayError:(NSError*)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activityIndicator stopAnimating];
        NSLog(@"Couldn't load the video :(");
        NSLog(@"%@", error);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[error userInfo] objectForKey:@"error"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
    });
}

-(void)playVidgeo:(PFObject*)videoData{
    // Convert the NSData from Parse into a video file
    [(PFFile*) videoData[kParseVideoData] getDataInBackgroundWithBlock:^(NSData *data, NSError *error){
        if (error)
        {
            NSLog(@" A problem when fetching the vidgeo data to display the video");
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self.movieURL = [ParseStore NSURLfromNSData:data];
            [self.activityIndicator stopAnimating];
            [self performSegueWithIdentifier:@"viewFavoriteVidgeo" sender:self];
        });
    }];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Unlock"]){
        //extract the password and unlock the vigdeo
        NSString *encryptedPassword = [ParseStore encryptPhrase:alertTextField.text];
        [self.activityIndicator startAnimating];
        [PFCloud callFunctionInBackground:@"unlockPublicVidgeo"
                           withParameters:@{@"vidgeo": vidgeoObject.objectId, @"passphrase":encryptedPassword}
                                    block:^(PFObject *videoObject, NSError *error) {
                                        if (error) {
                                            [self displayError:error];
                                        }else if(videoObject){
                                            [self playVidgeo:videoObject];
                                        }else{
                                            NSLog(@"Failed to unlock vidgeo!");
                                        }
                                    }];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"viewFavoriteVidgeo"])
    {
        [segue.destinationViewController setVideoURL:self.movieURL];
        [segue.destinationViewController setVidgeoObject:vidgeoObject];
        //        [segue.destinationViewController setVidgeoData:vidgeoData];
        
    }
}
//
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    //Where we configure the cell in each row
//
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell;
//
//    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//    // Configure the cell... setting the text of our cell's label
//    PFObject* vidgeo=[self.vidgeos objectAtIndex:indexPath.row];
//    cell.textLabel.text = [ParseStore stringFromDate:vidgeo.createdAt ofType:@"dd-MMM-yyyy HH:mm"] ;
//    return cell;
//}
//


@end
