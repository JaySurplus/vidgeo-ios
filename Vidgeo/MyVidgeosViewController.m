//
//  MyVidgeosViewController.m
//  Vidgeo
//
//  Created by John Mercouris on 2/24/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "MyVidgeosViewController.h"
#import "UserVidgeoViewController.h"
#import "ParseStore.h"
#import "vidgeoCell.h"

@implementation MyVidgeosViewController
@synthesize vidgeos;
@synthesize movieURL;
@synthesize myVidgeosCollectionView;
NSArray * tempVidgeos;
PFObject * vidgeoData;
PFUser * user;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [myVidgeosCollectionView setDelegate:self];
    [myVidgeosCollectionView setDataSource:self];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.vidgeos = [[NSArray alloc] init];
    [self.activityIndicator startAnimating];
    [PFCloud callFunctionInBackground:@"thumbnailForUserVidgeos"
                       withParameters:@{}
                                block:^(NSArray *myvidgeos, NSError *error) {
                                    if (error) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [self.activityIndicator stopAnimating];
                                        });
                                        NSLog(@"Couldn't display my vidgeos!");
                                        NSLog(@"%@", error);
                                    }else if(myvidgeos){
                                        tempVidgeos=myvidgeos;
                                        [self setUpImages:tempVidgeos];
                                    }else{
                                        NSLog(@"Failed to fetch the vidgeo locations!");
                                    }
                                }];
}

// -- Method fetches all the image files for each vidgeo and stores them localy
- (void)setUpImages:(NSArray *)images
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        // Iterate over all images and get the data from the PFFile
        for (int i = 0; i < images.count; i++)
        {
            PFObject *eachObject = [images objectAtIndex:i];
            PFFile *theImage = [eachObject objectForKey:kParseThumbnail];
            NSData *imageData = [theImage getData];
            if (imageData && ![imageData isEqual:[NSNull null]] && imageData!=nil) {
                [tempVidgeos[i] setObject:imageData forKey:kVidgeoThumbnail];
            }
        }
        self.vidgeos=tempVidgeos;
        // Update UI on main thread.
        dispatch_async(dispatch_get_main_queue(), ^{
            [myVidgeosCollectionView reloadData];
            [self.activityIndicator stopAnimating];
        });
    });
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.vidgeos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    vidgeoCell *cell = (vidgeoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.vidgeoThumbnail.image=nil;
    [cell.lockIcon setHidden:YES];
    if (self.vidgeos[indexPath.row][kVidgeoLock]) {
        [cell.vidgeoThumbnail setImage:[UIImage imageNamed:@"iTunesArtwork.png"]];
        [cell.lockIcon setHidden:NO];
    }else if (self.vidgeos[indexPath.row][kVidgeoThumbnail]) {
        UIImage *image = [UIImage imageWithData:self.vidgeos[indexPath.row][kVidgeoThumbnail]];
        UIImage * rotatedImage = [[UIImage alloc] initWithCGImage: image.CGImage
                                                            scale: 1.0
                                                      orientation: UIImageOrientationRight];
        [cell.vidgeoThumbnail setImage:rotatedImage];
    }else{
        [cell.vidgeoThumbnail setImage:[UIImage imageNamed:@"iTunesArtwork.png"]];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // When picture is touched, open a viewcontroller with the image
    [self.activityIndicator startAnimating];
    vidgeoData = self.vidgeos[indexPath.row];
    if (vidgeoData[kVidgeoLock]) {
        [PFCloud callFunctionInBackground:@"fetchUserLockedVidgeo"
                           withParameters:@{@"vidgeo": vidgeoData.objectId}
                                    block:^(PFObject *videoObject, NSError *error) {
                                        if (error) {
                                            [self errorDisplayingVidgeo:error];
                                        }else if(videoObject){
                                            [ParseStore updateViewCount:vidgeoData];
                                            [self playVidgeo:videoObject];
                                        }else{
                                            NSLog(@"Failed to save vidgeo view count!");
                                        }
                                    }];
    }else{
        [PFCloud callFunctionInBackground:@"videoForVidgeo"
                           withParameters:@{@"vidgeo": vidgeoData.objectId}
                                    block:^(PFObject *videoObject, NSError *error) {
                                        if (error) {
                                            [self errorDisplayingVidgeo:error];
                                            return;
                                        }else if(videoObject){
                                            [self playVidgeo:videoObject];
                                            return;
                                        }else{
                                            [self errorDisplayingVidgeo:error];
                                            NSLog(@"Failed to fetch the vidgeo locations!");
                                            return ;
                                        }
                                    }];
    }
}
-(void)errorDisplayingVidgeo:(NSError*)error{
    [self.activityIndicator stopAnimating];
    NSLog(@"Couldn't load the video :(");
    NSLog(@"%@", error);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[error userInfo] objectForKey:@"error"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alertView show];
    return;
}
-(void)playVidgeo:(PFObject*)videoData{
    // Convert the NSData from Parse into a video file
    [(PFFile*) videoData[kParseVideoData] getDataInBackgroundWithBlock:^(NSData *data, NSError *error){
        if (error)
        {
            NSLog(@" A problem when fetching the vidgeo data in playVidgeo method");
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^
         {
             self.movieURL = [ParseStore NSURLfromNSData:data];
             [self.activityIndicator stopAnimating];
             [self performSegueWithIdentifier:@"manageMyVidgeo" sender:self];
         }];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    //Where we configure the cell in each row
//
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell;
//
//    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//    // Configure the cell... setting the text of our cell's label
//    PFObject* vidgeo=[self.vidgeos objectAtIndex:indexPath.row];
//    cell.textLabel.text = [ParseStore stringFromDate:vidgeo.createdAt ofType:@"dd-MMM-yyyy HH:mm"] ;
//    return cell;
//}


// Prepare for Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"manageMyVidgeo"])
    {
        [segue.destinationViewController setVideoURL:self.movieURL];
        [segue.destinationViewController setVidgeoObject:vidgeoData];
        
    }
}

- (IBAction)unwindToMyVidgeos:(UIStoryboardSegue *)unwindSegue
{
    
    
}

@end