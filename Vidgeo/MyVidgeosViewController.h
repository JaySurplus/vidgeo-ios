//
//  MyVidgeosViewController.h
//  Vidgeo
//
//  Created by John Mercouris on 2/24/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParseStore.h"

@interface MyVidgeosViewController : UIViewController <UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
{
}
@property (nonatomic, strong) NSArray *vidgeos;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic)  NSURL *movieURL;
@property (strong, nonatomic) IBOutlet UICollectionView *myVidgeosCollectionView;

@end
