//
//  RecordVidgeoViewController.m
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 2/26/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//
#import "ParseStore.h"
#import "RecordVidgeoViewController.h"
#import "UserVidgeoViewController.h"
#import "Reachability.h"
#import "VideoConverter.h"

@interface RecordVidgeoViewController ()
{
    
}
@end

@implementation RecordVidgeoViewController
@synthesize currentLocation;
@synthesize vidgeoPicker, movieController;
PFObject *vidgeoInfo;
NSData * imageData;
@synthesize activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // Notification Center
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.movieController];
    
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDelegate:self];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    // We don't want to overload the user with multiple
    // updates, so we only update every 3 meters (at most).
    [_locationManager setDistanceFilter:3];
    
    if ([self.type isEqualToString:kVideoRecord]) {
        self.type=nil;
        NSLog(@"Trying to take vidgeo for the first time");
        [self takeVidgeo];
    }
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"This did load method is being called");
	// Do any additional setup after loading the view.
    // Notification Center
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -  MoviePlayer methods
// Movie Playback Finished
- (void)moviePlayBackDidFinish:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

-(void)takeVidgeo{
    // blackscreen when opening camera problem
    //apparentl b/c there are other threads running in background when this gets called
    // or becuase of video permissions problem.. i'm not sure
    //http://stackoverflow.com/questions/18930436/ios-7-uiimagepickercontroller-camera-no-image
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        self.vidgeoPicker = [[UIImagePickerController alloc] init];
        self.vidgeoPicker.delegate = self;
        self.vidgeoPicker.allowsEditing = YES;
        self.vidgeoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.vidgeoPicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        [self.vidgeoPicker setVideoMaximumDuration:11];
        [self.vidgeoPicker setVideoQuality:UIImagePickerControllerQualityTypeMedium];
        
        [self presentViewController:self.vidgeoPicker animated:YES completion:NULL];
    }];
}

// Video Controller
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"attempting to save the video");
    self.movieURL = info[UIImagePickerControllerMediaURL];
    
    //code for generating thumbnails
    AVAsset *asset = [AVAsset assetWithURL:self.movieURL];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = CMTimeMake(5, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    imageData = UIImageJPEGRepresentation(thumbnail, 0.05f);
    
#warning check if the user has internet before attempting to upload, if not save the video for later when they do have internet
    NetworkStatus networkStatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"Error: Do not have Internet at the moment to upload vidgeo!");
        [[[UIAlertView alloc] initWithTitle:@"No Internet Connection" message:@"Please make sure you have a Wi-Fi or Cellular connection and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }else{
        [self startActivityIndicator];
        [self movToMp4:self.movieURL];
    }
    
}

// Handle Video Take Cancel
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:NULL];
    [self dismissViewControllerAnimated:NO completion:nil];
    NSLog(@"Cancel button was pressed");
}

#pragma mark - vidgeo converter
-(void)movToMp4:(NSURL*)videoURL{
    //Thank you random person from stackoverflow:
    //http://stackoverflow.com/questions/20282672/record-save-and-or-convert-video-in-mp4-format
    
    NSString* mp4Path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"video.mp4"];
    //cleaning up the space
    NSError *error1;
    if([[NSFileManager defaultManager] fileExistsAtPath:mp4Path]){
        [[NSFileManager defaultManager] removeItemAtPath:mp4Path error:&error1];
        if(error1)
            NSLog(@"%@",error1);
    }
    
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:self.movieURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset presetName:AVAssetExportPresetPassthrough];
    exportSession.outputURL = [NSURL fileURLWithPath:mp4Path];
    NSLog(@"videopath of your mp4 file = %@",mp4Path);  // PATH OF YOUR .mp4 FILE
    exportSession.outputFileType = AVFileTypeMPEG4;
    NSLog(@"about to convert to mp4");
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        
        switch ([exportSession status]) {
                
            case AVAssetExportSessionStatusFailed:
                NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                
                break;
                
            case AVAssetExportSessionStatusCancelled:
                
                NSLog(@"Export canceled");
                
                break;
                
            default:
                
                break;
                
        }
        NSLog(@"about to present view controller");

        self.movieURL = [NSURL fileURLWithPath:mp4Path];
        [self upLoadVidgeo];
    }];
    return ;
}

-(void)upLoadVidgeo{
    NSData *movieData;
    NSError *dataReadingError = nil;
    movieData = [NSData dataWithContentsOfURL: self.movieURL options:NSDataReadingMapped error:&dataReadingError];
    if(movieData != nil)
        NSLog(@"Successfully loaded to NSdata.");
    else
        NSLog(@"Failed to load to NSdata with error = %@", dataReadingError);
    
    //Write video to a PFFile
    PFFile *videoFile = [PFFile fileWithName:@"video.mp4" data:movieData];
    PFFile *imageFile = [PFFile fileWithName:@"thumbnail.jpg" data:imageData];
    
    
    //store the location info in a seperate Table (class, whatever Parse)
    vidgeoInfo = [PFObject objectWithClassName:kVidgeoData];
    [vidgeoInfo setObject:self.currentLocation forKey:kParseLocation];
    [vidgeoInfo setObject:[PFUser currentUser] forKey:kParseUser];
    [vidgeoInfo setObject:videoFile forKey:kParseVideoData];
    [vidgeoInfo setObject:imageFile forKey:kParseThumbnail];
    [vidgeoInfo setObject:@0 forKey:kParseViewCount];
    [vidgeoInfo setObject:@[] forKey:kParseRateDown];
    [vidgeoInfo setObject:@[] forKey:kParseRateUp];
    //save this using cloudcode
    [vidgeoInfo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            [self stopActivityIndicator];
            NSLog(@"Couldn't save the vidgeo info!");
            NSLog(@"%@", error);
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[error userInfo] objectForKey:@"error"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alertView show];
            return;
        }
        if (succeeded) {
            [self stopActivityIndicator];
            [self.vidgeoPicker dismissViewControllerAnimated:NO completion:^(void)
             {
                 [self performSegueWithIdentifier:@"manageyMyVidgeo" sender:self];
             }];
            NSLog(@"%@", vidgeoInfo);
        } else {
            [self stopActivityIndicator];
            NSLog(@"Failed to save vidgeo info!");
        }
    }];

}

-(void)startActivityIndicator{
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.alpha = 1.0;
    CGRect bounds = [[UIApplication sharedApplication] keyWindow].bounds;
    self.activityIndicator.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));;
    self.activityIndicator.hidesWhenStopped = YES;
    [[[UIApplication sharedApplication] keyWindow] addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

-(void)stopActivityIndicator{
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}

// Prepare for Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"manageyMyVidgeo"])
    {
        [segue.destinationViewController setVideoURL:self.movieURL];
        [segue.destinationViewController setVidgeoObject:vidgeoInfo];
        [segue.destinationViewController setFromVC:@"Record"];
        
    }
}

@end
