//
//  SharingViewController.h
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 3/7/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import "FindFriendsViewController.h"
#import "ParseStore.h"

@interface SharingViewController : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@property (nonatomic, strong) UIPopoverController * popControl;

- (void)sendRequest:(PFObject*) vidgeoInfo;
-(void)postOnWall:(PFObject *)vidgeoInfo;
-(void)shareLocation:(PFObject *)vidgeoInfo;
@end