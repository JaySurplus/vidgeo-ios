//
//  VidgeoVC.m
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 10/31/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import "PublicVidgeoViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ParseStore.h"
#import "User.h"

@implementation PublicVidgeoViewController
MPMoviePlayerViewController* movieViewController;

@synthesize vidgeoObject,vidgeoData, videoView, videoURL, tabBar;
@synthesize upVotes,downVotes, dateLabel, overlayInfo;
UITabBarItem* currentItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tabBar setDelegate:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //start playing the video:
    [self.view bringSubviewToFront:self.videoView];
    [movieViewController.moviePlayer prepareToPlay];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(handleSingleTap:)];
    
    UIView *aView = [[UIView alloc] initWithFrame:movieViewController.moviePlayer.view.bounds];
    
    [aView addGestureRecognizer:tapGesture];
    [movieViewController.moviePlayer.view addSubview:aView];
    
    [movieViewController.moviePlayer play];
    
    // Add Movie to History
    //This was causing problems with the vidgeo sent from FB friends
    // need to do a deep copy of the object before adding the video
    //    User* singletonUser = [User sharedUser];
    //    PFObject* modVidgeoObject =  self.vidgeoObject;
    //    [modVidgeoObject addObject:self.vidgeoData forKey:kParseVideoData];
    
    //    [singletonUser addHistoryItem:modVidgeoObject];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    movieViewController =[[MPMoviePlayerViewController alloc] initWithContentURL:self.videoURL];
    movieViewController.moviePlayer.controlStyle = MPMovieControlStyleNone;
    [self.videoView addSubview:movieViewController.view];
    [movieViewController.view setFrame:self.videoView.bounds];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doneButtonClick:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    self.dateLabel.text = [ParseStore stringFromDate:self.vidgeoObject.createdAt ofType:@"dd-MMM-yyyy"];
    
    //getting the number of ratings
    [self displayDownRating:0];
    [self displayUpRating:0];
    if ([self.vidgeoObject[kParseViewCount] isEqual:[NSNull null]]) {
        self.vidgeoObject[kParseViewCount] = @0;
    }
    [self displayViewCount:[self.vidgeoObject[kParseViewCount] intValue]];
    
}

-(void)showLoginScreen
{
    // Create the log in view controller
    MyLogInViewController *logInViewController = [[MyLogInViewController alloc] init];
    [logInViewController setDelegate:self]; // Set ourselves as the delegate
    
    // Create the sign up view controller
    MySignUpViewController *signUpViewController = [[MySignUpViewController alloc] init];
    [signUpViewController setDelegate:self]; // Set ourselves as the delegate
    
    // Assign our sign up controller to be displayed from the login controller
    [logInViewController setSignUpController:signUpViewController];
    
    // Present the log in view controller
    [self presentViewController:logInViewController animated:YES completion:NULL];
}

-(void)doneButtonClick:(NSNotification*)aNotification
{
    [self.view bringSubviewToFront:self.overlayInfo];
    [self.overlayInfo setHidden:NO];
}

#pragma mark - tabBar controls
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    switch([item tag])
    {
            currentItem = item;
            // Case Back
        case kVideoBackButton:
            [self backTab];
            break;
            // Case Share
        case kVideoShareButton:
            [self shareTab];
            break;
            // Case Flag
        case kVideoFlagButton:
            [self flagTab];
            break;
            // Case Down Vote
        case kVideoVoteUpButton:
            [self upVoteTab];
            break;
            // Case Up Vote
        case kVideoVoteDownButton:
            [self downVoteTab];
            break;
        default:
            break;
    }
    if ([PFUser currentUser]) {
        [self.tabBar setSelectedItem:currentItem];
    }else{
        [self.tabBar setSelectedItem:nil];
    }
}

- (void)backTab
{
    // halt movie playback
    [movieViewController.moviePlayer stop];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)shareTab
{
    [[[UIAlertView alloc] initWithTitle:@"Share"
                                message:@""
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Share on Facebook",@"Share with select Friends",@"Share location", nil] show];
}

- (void)flagTab
{
    [[[UIAlertView alloc] initWithTitle:@"Flag this Vidgeo?"
                                message:@"Does this Vidgeo contain inappropriate content?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Yes, Flag this Vidgeo", nil] show];
}

- (void)upVoteTab
{
    if (![PFUser currentUser])
    {
        [self showLoginScreen];
    }
    else
    {
        [ParseStore rateVidgeoUp:self.vidgeoObject];
        [self displayUpRating:1];
        [[[tabBar items] objectAtIndex:kVideoVoteUpButton]setEnabled:FALSE];
    }
}


- (void)downVoteTab
{
    if (![PFUser currentUser])
    {
        [self showLoginScreen];
    }
    else
    {
        [ParseStore rateVidgeoDown:self.vidgeoObject];
        [self displayDownRating:1];
        [[[tabBar items] objectAtIndex:kVideoVoteDownButton]setEnabled:FALSE];
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Yes, Flag this Vidgeo"])
    {
        [ParseStore flagVidgeo:self.vidgeoObject];
        NSLog(@"flagged");
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Video Flagged", nil) message:NSLocalizedString(@"Video has been flagged for removal. (Please refresh map using \"user location button\")", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
    else if([title isEqualToString:@"Share on Facebook"]){
        
        [self postOnWall:self.vidgeoObject];
    }
    else if([title isEqualToString:@"Share location"]){
        [self shareLocation:self.vidgeoObject];
    }
    else if([title isEqualToString:@"Share with select Friends"]){
        [self sendRequest:self.vidgeoObject];
    }
}

// Did recieve memory warning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)swipeLeft:(id)sender
{
    [self backTab];
}

- (IBAction)swipeUp:(id)sender
{
    [self backTab];
}
- (IBAction)playButton:(id)sender {
    [self handleScreenTouch];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self handleScreenTouch];
}
- (void)handleScreenTouch{
    float rate = [movieViewController.moviePlayer currentPlaybackRate];
    if (rate ==0.0f) {
        [self.overlayInfo setHidden:YES];
        [self.view sendSubviewToBack:self.overlayInfo];
        [movieViewController.moviePlayer play];
    }else{
        [movieViewController.moviePlayer pause];
        [self.view bringSubviewToFront:self.overlayInfo];
        [self.overlayInfo setHidden:NO];
    }
}

#pragma mark - overlay buttons for more info buttons
- (IBAction)upVotesList:(id)sender {
    //code to display the upvote list
    
}
- (IBAction)downVotesList:(id)sender {
    //code to display the downvote list
    
}
- (IBAction)displayUploaderInfo:(id)sender {
    //code here to fetch the username of the vidgeo Creator and display it
    PFUser *creator=[self.vidgeoObject objectForKey:kParseUser];
    [PFCloud callFunctionInBackground:@"usernameForID"
                       withParameters:@{@"userId": creator.objectId}
                                block:^(PFUser* uploader, NSError *error) {
                                    if (error) {
                                        NSLog(@"Unable to fetch the vidgeo uploader info : %@",error);
                                        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error fetching uploader's info" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                            [alertView show];
                                        }];
                                        return;
                                    }else if(uploader){
                                        NSString * username= [uploader username];
                                        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                                            [self.uploader setTitle:username forState:UIControlStateNormal];
                                        }];
                                    }else{
                                        NSLog(@"Failed to save vidgeo view count!");
                                    }
                                }];
    
}

-(void)displayUpRating:(int)newValue{
    int rateUp = (self.vidgeoObject[kParseRateUp]) ? (int)[(NSArray*)self.vidgeoObject[kParseRateUp] count] : 0;
    rateUp +=newValue;
    NSString *stringRating = [NSString stringWithFormat:@"%d",rateUp];
    [[[tabBar items] objectAtIndex:kVideoVoteUpButton]setBadgeValue:stringRating];
    self.upVotes.text = stringRating;
    if ([self.vidgeoObject[kParseRateUp] containsObject:[PFUser currentUser].objectId]) {
        [[[self.tabBar items] objectAtIndex:kVideoVoteUpButton]setEnabled:FALSE];
    }
}
-(void)displayDownRating:(int)newValue{
    int rateDown = (self.vidgeoObject[kParseRateDown]) ? (int)[(NSArray*)self.vidgeoObject[kParseRateDown] count] : 0;
    rateDown +=newValue;
    NSString *stringRating = [NSString stringWithFormat:@"%d",rateDown];
    [[[tabBar items] objectAtIndex:kVideoVoteDownButton]setBadgeValue:stringRating];
    self.downVotes.text = stringRating;
    if ([self.vidgeoObject[kParseRateDown] containsObject:[PFUser currentUser].objectId]) {
        [[[self.tabBar items] objectAtIndex:kVideoVoteDownButton]setEnabled:FALSE];
    }
}
-(void)displayViewCount:(int)count{
    
    //write code here to update the viewcount UI
    self.viewCount.text = [NSString stringWithFormat:@"%i",count];
}



@end
