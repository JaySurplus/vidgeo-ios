//
//  WatchListViewController.h
//  Vidgeo
//
//  Created by John Mercouris on 2/24/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParseStore.h"

@interface WatchListViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>
{
    PFUser * user;
}

@property (nonatomic, strong) NSArray *vidgeos;
@property (strong, nonatomic) IBOutlet UICollectionView *watchListCollectionView;

@end
