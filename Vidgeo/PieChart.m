//
//  PieChart.m
//  Vidgeo
//
//  Created by John Mercouris on 10/28/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import "PieChart.h"

@implementation PieChart
CGRect myRect;
int SIDELENGTH = 30;

static inline float radians(double degrees) { return degrees * M_PI / 180; }

- (UIImage*)getPieImage:(float) completion
{
    UIImage* image;
    if(completion == 100.0f)
    {
        image = [UIImage imageNamed:@"completedTaskIcon.png"];
    }
    else
    {
        CGSize cgSize = CGSizeMake(SIDELENGTH,SIDELENGTH);
        UIGraphicsBeginImageContextWithOptions(cgSize, NO, 4);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);

        
        // the image should have a clear background
        [[UIColor clearColor] set];
        CGRect myRect = CGRectMake(0.0f, 0.0f, SIDELENGTH, SIDELENGTH);
        UIRectFill(myRect);
        
        // color was hopefully set before this method called
        [[UIColor greenColor] set];
        
        // centre point is required
        CGFloat midx = SIDELENGTH/2;
        CGFloat midy = SIDELENGTH/2;
        // radius of the arc
        CGFloat radius = (SIDELENGTH/2) * 0.60f;
        
        // Pie Chart Background
        CGContextSetFillColor(context, CGColorGetComponents([[UIColor redColor] CGColor]));
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius,  radians(0), radians(360), 0);
        CGContextFillPath(context);
        
        // Pie Segment
        CGContextSetFillColor(context, CGColorGetComponents([[UIColor greenColor] CGColor]));
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx, midy);
        CGContextAddLineToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius,  radians(0), radians(360 * (completion / 100.0f)), 0);
        CGContextFillPath(context);
        
        // Pie Center Point
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius/2,  radians(0), radians(360), 0);
        CGContextFillPath(context);

        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }


    return image;
}

- (UIImage*)getCenteredImageWithRating:(float)rating available: (bool) available
{    
    UIImage* image;
    
    CGSize cgSize = CGSizeMake(SIDELENGTH,SIDELENGTH);
    UIGraphicsBeginImageContextWithOptions(cgSize, NO, 4);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    rating = rating/100.0f;
    
    
    // the image should have a clear background
    [[UIColor clearColor] set];
    CGRect myRect = CGRectMake(0.0f, 0.0f, SIDELENGTH, SIDELENGTH);
    UIRectFill(myRect);
    
    // color was hopefully set before this method called
    [[UIColor greenColor] set];
    
    // centre point is required
    CGFloat midx = SIDELENGTH/2;
    CGFloat midy = SIDELENGTH/2;
    // radius of the arc
    CGFloat radius = (SIDELENGTH/2) * 0.60f;
    
    // Coordinate Background
    if (available)
    {
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius,  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }
    else
    {
        CGContextSetFillColorWithColor(context, [UIColor grayColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius,  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }

    // Coordinate Fill
    if (rating >= 0.0f)
    {
        CGContextSetFillColorWithColor(context, [UIColor colorWithRed:0.0f green:1.0f blue:0.0f alpha:rating].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius/2,  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }
    else if (rating < 0.0f)
    {
        rating = fabsf(rating);
        CGContextSetFillColorWithColor(context, [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:rating].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius/2,  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }
    

    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

- (UIImage*)getCenteredImageWithPopularity: (float) popularity available:(bool)available
{
    if (popularity > 100)
    {
        popularity = 100;
    }
    else if(popularity < 100)
    {
        popularity = 0;
    }
    
    UIImage* image;
    
    CGSize cgSize = CGSizeMake(SIDELENGTH,SIDELENGTH);
    UIGraphicsBeginImageContextWithOptions(cgSize, NO, 4);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    
    // the image should have a clear background
    [[UIColor clearColor] set];
    CGRect myRect = CGRectMake(0.0f, 0.0f, SIDELENGTH, SIDELENGTH);
    UIRectFill(myRect);
    
    // color was hopefully set before this method called
    [[UIColor greenColor] set];
    
    // centre point is required
    CGFloat midx = SIDELENGTH/2;
    CGFloat midy = SIDELENGTH/2;
    // radius of the arc
    CGFloat radius = (SIDELENGTH/2) * 0.60f;
    
    // Coordinate Background
    if (available)
    {
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius,  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }
    else
    {
        CGContextSetFillColorWithColor(context, [UIColor grayColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius,  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }
    
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, midx + radius, midy);
    CGContextAddArc(context, midx, midy, (radius/2) + (popularity/25),  radians(0), radians(360), 0);
    CGContextFillPath(context);
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage*)getCenteredImageWithRating:(float)rating popularity:(float)popularity available:(bool)available
{
    UIImage* image;
    
    CGSize cgSize = CGSizeMake(SIDELENGTH,SIDELENGTH);
    UIGraphicsBeginImageContextWithOptions(cgSize, NO, 4);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    rating = rating/100.0f;
    
    // the image should have a clear background
    [[UIColor clearColor] set];
    CGRect myRect = CGRectMake(0.0f, 0.0f, SIDELENGTH, SIDELENGTH);
    UIRectFill(myRect);
    
    // color was hopefully set before this method called
    [[UIColor greenColor] set];
    
    // centre point is required
    CGFloat midx = SIDELENGTH/2;
    CGFloat midy = SIDELENGTH/2;
    // radius of the arc
    CGFloat radius = (SIDELENGTH/2) * 0.60f;
    
    // Coordinate Background
    if (available)
    {
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius,  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }
    else
    {
        CGContextSetFillColorWithColor(context, [UIColor grayColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, radius,  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }
    
    // Coordinate Fill
    if (rating > 0.0f)
    {
        CGContextSetFillColorWithColor(context, [UIColor colorWithRed:0.0f green:1.0f blue:0.0f alpha:rating].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, (radius/2) + (popularity/25),  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }
    else if (rating < 0.0f)
    {
        rating = fabsf(rating);
        CGContextSetFillColorWithColor(context, [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:rating].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, midx + radius, midy);
        CGContextAddArc(context, midx, midy, (radius/2) + (popularity/25),  radians(0), radians(360), 0);
        CGContextFillPath(context);
    }
        
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end