//
//  AnimatedCircleView.h
//  Vidgeo
//
//  Created by John Mercouris on 3/5/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface AnimatedCircleView : MKCircleView
{
    UIImageView* imageView;
}

-(void)start;
-(void)stop;

@end
