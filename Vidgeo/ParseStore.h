//
//  ParseStore.h
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 10/26/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

// Parse API key constants:
static NSString * const kParseVideoData = @"videoData";
static NSString * const kParseVidgeoID = @"vidgeoID";
static NSString * const kParseVideoID = @"videoID";
static NSString * const kParseThumbnail = @"thumbnail";
static NSString * const kParseRateUp = @"rateUp";
static NSString * const kParseRateDown = @"rateDown";
static NSString * const kParseViewCount = @"viewCount";
static NSString * const kVidgeoLock = @"lockedVidgeo";
static NSString * const kParseText = @"text";
static NSString * const kParseLocation = @"location";
static NSString * const kParseFlagged = @"flagged";
static NSString * const kParseUser = @"user";
static NSString * const kParseTime = @"createdAt";
static NSString * const kParseFriends = @"friendlist";

static NSString * const kVidgeoStore = @"VidgeoStore";
static NSString * const kVidgeoData = @"dataStore";
static NSString * const kVidgeoLocation = @"dataStore";
static NSString * const kVidgeoThumbnail = @"ThumbnailStore";
static NSString * const kVidgeoVersion = @"version";

static NSString * const kShareVidgeo = @"sharedVidgeo";

@interface ParseStore : NSObject
@property (strong, atomic) NSString *UUID;


+(NSMutableArray*)getFriends;

// methods for posting updates
+(void)rateVidgeoUp:(PFObject*)vidgeoID;
+(void)rateVidgeoDown:(PFObject*)vidgeoID;
+(void)flagVidgeo:(PFObject*)vidgeo;
+(void)updateViewCount:(PFObject*)vidgeo;
+(void)lockVidgeo:(PFObject*)vidgeo withPasscode:(NSString*)phrase;

//methods for modifying a Vidgeo
+(void)deleteMyVidgeo:(PFObject*)myVidgeo;

+(void)addFriend:(NSString*)fbID;
+(NSString*)stringFromDate:(NSDate*)vidgeoDate ofType:(NSString*)type;

+(NSURL *)NSURLfromNSData:(NSData*)videoData;

+(NSString*)encryptPhrase:(NSString*)phrase;

@end
