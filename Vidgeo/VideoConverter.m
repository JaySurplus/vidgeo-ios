//
//  VideoConverter.m
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 3/28/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "VideoConverter.h"
#import <AVFoundation/AVFoundation.h>

@implementation VideoConverter
+(NSURL*)movToMp4:(NSURL*)videoURL{
    NSString* mp4Path;
    //Thank you random person from stackoverflow:
    //http://stackoverflow.com/questions/20282672/record-save-and-or-convert-video-in-mp4-format
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *videoPath1 =[NSString stringWithFormat:@"%@/xyz.mov",docDir];
    NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
    [videoData writeToFile:videoPath1 atomically:NO];
    //  UISaveVideoAtPathToSavedPhotosAlbum(moviePath, self, nil, nil);
    
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:videoPath1] options:nil];
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset presetName:AVAssetExportPresetPassthrough];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    mp4Path = [NSString stringWithFormat:@"%@/xyz.mp4", [paths objectAtIndex:0]];
    exportSession.outputURL = [NSURL fileURLWithPath:mp4Path];
    NSLog(@"videopath of your mp4 file = %@",mp4Path);  // PATH OF YOUR .mp4 FILE
    exportSession.outputFileType = AVFileTypeMPEG4;
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        
        switch ([exportSession status]) {
                
            case AVAssetExportSessionStatusFailed:
                NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                
                break;
                
            case AVAssetExportSessionStatusCancelled:
                
                NSLog(@"Export canceled");
                
                break;
                
            default:
                
                break;
                
        }
        UISaveVideoAtPathToSavedPhotosAlbum(mp4Path, self, nil, nil);
        
    }];
    
   return [NSURL fileURLWithPath:mp4Path];

    
}
@end
