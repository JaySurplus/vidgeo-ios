//
//  UserVidgeoViewController.h
//  Vidgeo
//
//  Created by John Mercouris on 2/24/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AddressBook/AddressBook.h>
#import "SharingViewController.h"
#import "ParseStore.h"

static int const kVideoBackButton = 0;
static int const kVideoLockButton = 1;
static int const kVideoShareButton = 2;
static int const kVideoDelete = 3;


@interface UserVidgeoViewController : SharingViewController <UITabBarDelegate,CLLocationManagerDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong, nonatomic) IBOutlet UIImageView *eyeImage;
@property (strong, nonatomic) IBOutlet UILabel *viewsLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *viewCount;
@property (strong, nonatomic) IBOutlet UILabel *downVotes;
@property (strong, nonatomic) IBOutlet UILabel *upVotes;
@property (strong, nonatomic) IBOutlet UIView *overLay;
@property (strong, nonatomic) PFObject *vidgeoObject;
@property (strong, nonatomic) NSURL *videoURL;
@property  (strong, nonatomic) NSString *fromVC;
- (IBAction)swipeLeft:(id)sender;
- (IBAction)swipeUp:(id)sender;
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer;

@end