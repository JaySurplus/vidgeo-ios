//
//  UserAgreementViewController.h
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 12/5/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserAgreementViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *IAgreeButton;

@end
