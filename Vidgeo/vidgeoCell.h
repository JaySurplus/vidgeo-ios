//
//  vidgeoCell.h
//  Vidgeo
//
//  Created by John Mercouris on 3/2/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface vidgeoCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *vidgeoThumbnail;
@property (strong, nonatomic) IBOutlet UIImageView *lockIcon;

@end