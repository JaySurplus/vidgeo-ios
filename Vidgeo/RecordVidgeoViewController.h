//
//  RecordVidgeoViewController.h
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 2/26/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <MapKit/MapKit.h>
#import "ParseStore.h"


static NSString * const kVideoRecord = @"Record";

@interface RecordVidgeoViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate, CLLocationManagerDelegate>

@property (strong,   nonatomic) NSURL *movieURL;
@property (strong, nonatomic) MPMoviePlayerController *movieController;
@property (strong, nonatomic) UIImagePickerController *vidgeoPicker;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) PFGeoPoint *currentLocation;
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
-(void)takeVidgeo;
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;

@end
