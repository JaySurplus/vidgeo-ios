//
//  vidgeoCell.m
//  Vidgeo
//
//  Created by John Mercouris on 3/2/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "vidgeoCell.h"

@implementation vidgeoCell
@synthesize vidgeoThumbnail;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end