//
//  HistoryViewController.m
//  Vidgeo
//
//  Created by John Mercouris on 3/3/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "HistoryViewController.h"
#import "vidgeoCell.h"
#import "User.h"
#import "ParseStore.h"
#import "PublicVidgeoViewController.h"

@interface HistoryViewController ()
@end


@implementation HistoryViewController
User* singletonUser;
PFObject * vidgeoData;
NSArray * vidgeoThumbnails;
NSArray * vidgeoHistory;



@synthesize historyCollectionView, movieURL;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [historyCollectionView setDataSource:self];
    [historyCollectionView setDelegate:self];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    singletonUser = [User sharedUser];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Two queries here
        singletonUser = [User sharedUser];

    vidgeoHistory = [NSArray arrayWithArray:[singletonUser getHistory]];
    NSArray* vidgeoIDs = [vidgeoHistory valueForKey:kParseVideoID];
    PFQuery *query = [PFQuery queryWithClassName:kVidgeoThumbnail];
    [query whereKey:kParseVideoID containedIn:vidgeoIDs];
    [query orderByAscending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if (!error)
         {
             //[self.activityIndicator stopAnimating];
             [self setUpImages:objects];
         }
     }];
}
- (void)setUpImages:(NSArray *)images
{

    NSMutableArray *imageDataArray = [NSMutableArray array];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        // Iterate over all images and get the data from the PFFile
        for (int i = 0; i < images.count; i++)
        {
            PFObject *eachObject = [images objectAtIndex:i];
            PFFile *theImage = [eachObject objectForKey:kParseVideoData];
            NSData *imageData = [theImage getData];
            UIImage *image = [UIImage imageWithData:imageData];
            UIImage * rotatedImage = [[UIImage alloc] initWithCGImage: image.CGImage
                                                                scale: 1.0
                                                          orientation: UIImageOrientationRight];
            
            [imageDataArray addObject:rotatedImage];
        }
        vidgeoThumbnails = imageDataArray;
        // Update UI on main thread.
        dispatch_async(dispatch_get_main_queue(), ^{
            [historyCollectionView reloadData];
        });
    });
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [vidgeoThumbnails count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    vidgeoCell *cell = (vidgeoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
//    cell.vidgeoThumbnail.image=nil;
 
    [cell.vidgeoThumbnail setImage:[vidgeoThumbnails objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // When picture is touched, open a viewcontroller with the image
    vidgeoData = (PFObject*)vidgeoHistory[indexPath.row];
    self.movieURL = [ParseStore NSURLfromNSData:vidgeoData[kParseVideoData]];
    [self performSegueWithIdentifier:@"viewMyHistoryVidgeo" sender:self];
}

// Prepare for Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"viewMyHistoryVidgeo"])
    {
        [segue.destinationViewController setVideoURL:self.movieURL];
        [segue.destinationViewController setVidgeoObject:vidgeoData];
        [segue.destinationViewController setVidgeoData:vidgeoData[kParseVideoData]];        
    }
}

- (IBAction)unwindToMyVidgeos:(UIStoryboardSegue *)unwindSegue
{
    
    
}


@end
