//
//  MyLogInViewController.m
//  Vidgeo
//
//  Created by John Mercouris on 11/3/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import "MyLogInViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface MyLogInViewController ()
@property (nonatomic, strong) UIImageView *fieldsBackground;
@end

@implementation MyLogInViewController

@synthesize fieldsBackground;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Background Color
    float rd = 254.00/255.00;
    float gr = 193.00/255.00;
    float bl = 66.00/255.00;
    [self.logInView setBackgroundColor:[UIColor colorWithRed:rd green:gr blue:bl alpha:1.0]];
    [self.logInView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"colorpattern.png"]]];

    // Set Logo Image
    [self.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vidgeo-icon.png"]]];
    
    // Set Skip Button Appearance
    [self.logInView.dismissButton setImage:[UIImage imageNamed:@"skip-button.png"] forState:UIControlStateNormal];
    [self.logInView.dismissButton setImage:[UIImage imageNamed:@"skip-button.png"] forState:UIControlStateHighlighted];
    
    // Remove Login Button/ Additional Text
    [self.logInView.logInButton removeFromSuperview];
    [self.logInView.signUpLabel removeFromSuperview];
    
    [self.logInView.passwordForgottenButton setBackgroundImage:nil forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.logInView.passwordForgottenButton setTitle:@"forgot your password?" forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setTitle:@"forgot your password?" forState:UIControlStateHighlighted];

    // Set Sign up Button Appearance
    [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@"signup-button.png"] forState:UIControlStateNormal];
    [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@"signup-button.png"] forState:UIControlStateHighlighted];
    [self.logInView.signUpButton setTitle:@"" forState:UIControlStateNormal];
    [self.logInView.signUpButton setTitle:@"" forState:UIControlStateHighlighted];
    
    // Add login field background
    fieldsBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LoginFieldBG.png"]];
    [self.logInView addSubview:self.fieldsBackground];
    [self.logInView sendSubviewToBack:self.fieldsBackground];
    
    // Remove text shadow
    CALayer *layer = self.logInView.usernameField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.logInView.passwordField.layer;
    layer.shadowOpacity = 0.0f;
    
    // Set field text color
    [self.logInView.usernameField setTextColor:[UIColor blackColor]];
    [self.logInView.passwordField setTextColor:[UIColor blackColor]];
    
    self.logInView.usernameField.returnKeyType = UIReturnKeyGo;
    self.logInView.passwordField.returnKeyType = UIReturnKeyGo;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    float width = self.view.window.frame.size.width;
    float height = self.view.window.frame.size.height;
    float scaleFactor = width/640.0f;
    
    // X, Y, Width, Height
    // Layout Logo
    [self.logInView.logo setFrame:CGRectMake((width/2 - 500.0f*scaleFactor/2), (height/10), 500.0f*scaleFactor, 200.0f*scaleFactor)];
    // Layout Username Field
    [self.logInView.usernameField setFrame:CGRectMake(0.0f, (height/10)*3, width, 50.0f)];
    // Layout Password Field
    [self.logInView.passwordField setFrame:CGRectMake(0.0f, (height/10)*3+50, width, 50.0f)];
    // Layout Field Backgrounds
    [self.fieldsBackground setFrame:CGRectMake(0.0f, (height/10)*3, width, 100.0f)];
    // Layout Password Reset Button
    [self.logInView.passwordForgottenButton setFrame:CGRectMake((width/2 - 80), (height/10)*4.8, 250.0f, 50.0f)];
    // Layout Sign Up Button
    [self.logInView.signUpButton setFrame:CGRectMake((width/2 - 384.0*scaleFactor/2), (height/10)*6.5, 384*scaleFactor, 96*scaleFactor)];
    // Layout Skip Button
    [self.logInView.dismissButton setFrame:CGRectMake((width/2 - 384.0*scaleFactor/2), (height/10)*8, 384*scaleFactor, 96*scaleFactor)];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end