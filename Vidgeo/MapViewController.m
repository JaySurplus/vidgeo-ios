//
//  ViewController.m
//  Vidgeo
//
//  Created by John Mercouris on 10/26/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import "PublicVidgeoViewController.h"
#import "UserAgreementViewController.h"
#import "RecordVidgeoViewController.h"
#import "MapViewController.h"
#import "NSString+MD5.h"
#import "User.h"
#import "Constant.h"
#import "DrivingDirections.h"

@implementation MapViewController
@synthesize globalMapView, vidgeoList, locationMapping;
MKAnnotationView *selectedView;
bool opened = false;
MKCircle *circle;
NSData* vidgeoData;
int viewableRadius = 50000;
int recencyTimeInterval = 86400*4;
User *singletonInstanceUser;
MKAnnotationView *selectedView;
PFObject *selectedVidgeo;
PFGeoPoint *currentLocation;
BOOL didAttemptLogin;
UITextField * alertTextField;


/* Generate MD5
 NSString *private = @"CzyJ2qMMW1DyQ7ol2VTXB3lhwNFEyPEqQHFrY7yIpIA";
 NSString *md5String = [NSString stringWithFormat:@"%@%@", public, private];
 
 return [md5String MD5];
 */


- (void)viewDidLoad {
    opened = false;
    [super viewDidLoad];
    // [self updateVidgeoLocations:globalMapView.userLocation.coordinate];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.globalMapView setDelegate:self];
    
    singletonInstanceUser = [User sharedUser];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.globalMapView removeOverlays:self.globalMapView.overlays];
    circle = [MKCircle circleWithCenterCoordinate:self.globalMapView.userLocation.coordinate radius:viewableRadius];
    [self.globalMapView addOverlay:circle];
    
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDelegate:self];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    [_locationManager setDistanceFilter:100];
    
    if (![PFUser currentUser] && !didAttemptLogin)
    {
        [self showLoginScreen];
        didAttemptLogin = TRUE;
    }else{
        [User updateVersion:[PFUser currentUser]];
    }
    
    // Notification Center
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.movieController];
    self.vidgeoList = [[NSMutableArray alloc] init];
    // Update Pins Everytime View Appears
    //    [self updateVidgeoLocations:self.globalMapView.userLocation.coordinate];
    //a hack to make sure that vidgeos are loaded when you come back from another VC
    if (!opened) {
        // Force to update user location
        [self updateVidgeoLocations:self.globalMapView.userLocation.coordinate];
    }
    
}

- (void)showEULA {
    BOOL userAgreement = (BOOL)[[NSUserDefaults standardUserDefaults] objectForKey:@"USERAGREEMENT"];
    if (!userAgreement) {
        NSString * storyboardName = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"user_agreement"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (IBAction)showHomeOptions:(id)sender {
    if ([PFUser currentUser]) {
        [User updateVersion:[PFUser currentUser]];
        [self performSegueWithIdentifier:@"showHomeOptions" sender:self];
    }
    else {
        [self showLoginScreen];
    }
}

-(void)showLoginScreen {
    [self showEULA];
    // Create the log in view controller
    MyLogInViewController *logInViewController = [[MyLogInViewController alloc] init];
    [logInViewController setDelegate:self]; // Set ourselves as the delegate
    
    // Create the sign up view controller
    MySignUpViewController *signUpViewController = [[MySignUpViewController alloc] init];
    [signUpViewController setDelegate:self]; // Set ourselves as the delegate
    
    // Assign our sign up controller to be displayed from the login controller
    [logInViewController setSignUpController:signUpViewController];
    
    // Present the log in view controller
    [self presentViewController:logInViewController animated:YES completion:NULL];
}


#pragma mark - PFLogInViewControllerDelegate
// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length && password.length) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - PFSignUpViewControllerDelegate
// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    BOOL informationComplete = YES;
    // loop through all of the submitted data
    for (id key in info) {
        NSString *field = [info objectForKey:key];
        if (!field || !field.length)
        {   // check completion
            informationComplete = NO;
            break;
        }
    }
    
    // Display an alert if a field wasn't completed
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
    
    return informationComplete;
}

// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error {
    NSLog(@"Failed to sign up...");
}

// Sent to the delegate when the sign up screen is dismissed.
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    NSLog(@"User dismissed the signUpViewController");
}

#pragma mark -
#pragma mark MapKit Methods

// Drop Pins
-(void)dropPins:(NSMutableArray *)cllArray {
    //Clear all Previous Pins
    for (id annotation in self.globalMapView.annotations) {
        [self.globalMapView removeAnnotation:annotation];
    }
    // Create your coordinate
    CLLocationCoordinate2D myCoordinate = globalMapView.userLocation.coordinate;
    //Create your annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    // Set your annotation to point at your coordinate
    point.coordinate = myCoordinate;
    
    locationMapping = [[NSMutableDictionary alloc] init];
    // Drop pin on map
    for (PFObject* object in cllArray) {
        if (object[kParseFlagged]) {
            continue;
        }
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = CLLocationCoordinate2DMake([(PFGeoPoint*)object[kParseLocation] latitude], [(PFGeoPoint*)object[kParseLocation] longitude]);
        [self.globalMapView addAnnotation:point];
        
        //hack way to make it easier to map a annotation to the video info
        locationMapping[[NSString stringWithFormat:@"%lu",(unsigned long)point.hash]]=object;
    }
}

- (IBAction)longPress:(id)sender {
    [_locationManager stopUpdatingLocation];
    [_locationManager startUpdatingLocation];
    
    MKCoordinateRegion mapRegion;
    mapRegion = MKCoordinateRegionMakeWithDistance(
                                                   self.globalMapView.userLocation.coordinate,
                                                   100,
                                                   100);
    
    [self.globalMapView setRegion:mapRegion animated: YES];
}

// User Clicked Focus button
- (IBAction)userMapFocus:(id)sender {
    // Force to update user location
    [_locationManager stopUpdatingLocation];
    [_locationManager startUpdatingLocation];
    
    MKCoordinateRegion mapRegion;
    mapRegion = MKCoordinateRegionMakeWithDistance(
                                                   self.globalMapView.userLocation.coordinate,
                                                   viewableRadius * 2,
                                                   viewableRadius * 2);
    
    [self.globalMapView setRegion:mapRegion animated: YES];
    [self updateVidgeoLocations:self.globalMapView.userLocation.coordinate];
}

// Did update user location of user
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    // --- 1/4 of true viewable area, should be *1000 at end
    viewableRadius = (sqrt((userLocation.location.altitude * 100) / 6.752))*1000;
    if(viewableRadius < 1000) {
        viewableRadius = 1000;
    }
    
    [mapView removeOverlays:mapView.overlays];
    circle = [MKCircle circleWithCenterCoordinate:userLocation.coordinate radius:viewableRadius];
    [self.globalMapView addOverlay:circle];
    
    if(!opened) {
        opened = true;
        MKCoordinateRegion mapRegion;
        mapRegion.center = self.globalMapView.userLocation.coordinate;
        mapRegion.span.latitudeDelta = 1.0;
        mapRegion.span.longitudeDelta = 1.0;
        [globalMapView setRegion:mapRegion animated: YES];
        [self updateVidgeoLocations:self.globalMapView.userLocation.coordinate];
    }
}

//- (MKOverlayView *)mapView:(MKMapView *)map viewForOverlay:(id <MKOverlay>)overlay
//{
//    AnimatedCircleView* circleView = [[AnimatedCircleView alloc] initWithCircle:(MKCircle *)overlay];
//    return circleView;
//}

- (MKOverlayView *)mapView:(MKMapView *)map viewForOverlay:(id <MKOverlay>)overlay {
    MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
    circleView.strokeColor = [[UIColor whiteColor] colorWithAlphaComponent:.8f];
    
    return circleView;
}

// Pin Dropped
-(MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    // If User Location Pin; Do not Change
    if (annotation == mapView.userLocation) return nil;
    
    MKPinAnnotationView *tempPin=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"current"];
    tempPin.pinColor = MKPinAnnotationColorRed;
    PFObject* vidgeo = (PFObject*)locationMapping[[NSString stringWithFormat:@"%lu",(unsigned long)annotation.hash]];
    float rateUp = (vidgeo[kParseRateUp]) ? (float)[vidgeo[kParseRateUp] count] : 0.0f;
    float rateDown = (vidgeo[kParseRateDown]) ? (float)[vidgeo[kParseRateDown] count] : 0.0f;
    float viewcount = (vidgeo[kParseViewCount] && ![vidgeo[kParseViewCount] isEqual:[NSNull null]]) ? [vidgeo[kParseViewCount] floatValue] : 0.0f;
    PieChart *chart = [[PieChart alloc]init];
    
    tempPin.image = [chart getCenteredImageWithRating:(rateUp-rateDown+1)*80 popularity:viewcount available:YES];
    
    return tempPin;
}

-(void)updateVidgeoLocations:(CLLocationCoordinate2D)centeredOnLocation {
    // Crrently just getting all the video locations
    PFGeoPoint *screencenterGeoPoint = [PFGeoPoint geoPointWithLatitude:centeredOnLocation.latitude longitude:centeredOnLocation.longitude];
    [self.activityIndicator startAnimating];
    [PFCloud callFunctionInBackground:@"vidgeosNearUser"
                       withParameters:@{@"userLocation": screencenterGeoPoint}
                                block:^(NSArray *objects, NSError *error) {
                                    if (error) {
                                        NSLog(@"Unable access Vidgeos");
                                        NSLog(@"%@", error);
                                        [[[UIAlertView alloc] initWithTitle:[[error userInfo] objectForKey:@"error"]
                                                                    message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
                                        return;
                                    }else if(objects) {
                                        self.vidgeoList = [NSMutableArray arrayWithArray:objects];
                                        [[NSOperationQueue mainQueue] addOperationWithBlock:^
                                         {
                                             [self.activityIndicator stopAnimating];
                                             [self dropPins:self.vidgeoList];
                                         }];
                                    }else {
                                        NSLog(@"Failed to fetch the vidgeo locations!");
                                    }
                                }];
}
- (IBAction)refreshPins:(id)sender {
    [self updateVidgeoLocations:[self.globalMapView centerCoordinate]];
}


#pragma mark - play Vidgeo for pin methods
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    //if it's user location pin
    if (view.annotation == mapView.userLocation) return;
    
    selectedVidgeo = (PFObject*)locationMapping[[NSString stringWithFormat:@"%lu",(unsigned long)view.annotation.hash]];
    [self watchVidgeo:selectedVidgeo];
}

-(void)watchVidgeo:(PFObject*)selectedVideo {
    if (selectedVideo[kVidgeoLock]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Unlock this Vidgeo" message:@"This Vidgeo is locked, enter the secret code to unlock it:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Unlock",nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        alertTextField = [alert textFieldAtIndex:0];
        alertTextField.secureTextEntry = YES;
        alertTextField.keyboardType = UIKeyboardTypeAlphabet;
        alertTextField.placeholder = @"secret code here:";
        [alert show];
    } else {
        [self.activityIndicator startAnimating];
        [PFCloud callFunctionInBackground:@"videoForVidgeo"
                           withParameters:@{@"vidgeo": selectedVideo.objectId}
                                    block:^(PFObject *videoObject, NSError *error) {
                                        if (error) {
                                            [self displayVidgeoError:error];
                                            return;
                                        } else if(videoObject) {
                                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                                [self.activityIndicator stopAnimating];
                                                [self  playVidgeo:videoObject];
                                                return;
                                            }];
                                        } else {
                                            [self displayVidgeoError:error];
                                            NSLog(@"Failed to fetch the vidgeo locations!");
                                            return ;
                                        }
                                    }];
    }
}

-(void)displayVidgeoError:(NSError*)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activityIndicator stopAnimating];
        NSLog(@"Couldn't load the video :(");
        NSLog(@"%@", error);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[error userInfo] objectForKey:@"error"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
    });
}

-(void)playVidgeo:(PFObject*)videoObject {
    // Convert the NSData from Parse into a video file
    vidgeoData =[(PFFile*) videoObject[kParseVideoData] getData];
    self.movieURL = [ParseStore NSURLfromNSData:vidgeoData];
    //Play the video on main thread
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        [self.activityIndicator stopAnimating];
        [self performSegueWithIdentifier:@"showVidgeo" sender:self];
    }];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Directions"]) {
        [DrivingDirections openDirectionsWithLatitude:selectedView.annotation.coordinate.latitude longitude:selectedView.annotation.coordinate.longitude];
    }
    if([title isEqualToString:@"Unlock"]) {
        [self.activityIndicator startAnimating];
        //add encryption here
        NSString *encryptedPassword = [ParseStore encryptPhrase:alertTextField.text];
        [PFCloud callFunctionInBackground:@"unlockPublicVidgeo"
                           withParameters:@{@"vidgeo": selectedVidgeo.objectId, @"passphrase":encryptedPassword}
                                    block:^(PFObject *videoObject, NSError *error) {
                                        if (error) {
                                            [self displayVidgeoError:error];
                                        } else if(videoObject) {
                                            [self playVidgeo:videoObject];
                                        } else {
                                            NSLog(@"Failed to unlock the vidgeo!");
                                        }
                                    }];
    }
}

#pragma mark -
#pragma mark Filtering methods

// User Filter Pressed
- (IBAction)userFilter:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Filter By"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel Filter"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Popularity", @"Recent", nil];
    [actionSheet showInView:self.view];
}

// Action Sheet Pressed
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:@"Popularity"]) {
        [self filterByPopularity];
    }
    else if ([buttonTitle isEqualToString:@"Recent"]) {
        [self filterByRecency];
    }
    else if ([buttonTitle isEqualToString:@"Watch List"]) {
        NSLog(@"Watch List Clicked");
    }
    else if ([buttonTitle isEqualToString:@"Cancel Filter"]) {
        [self dropPins:self.vidgeoList];
    }
}

-(void)filterByPopularity
{
    NSMutableArray *popularList= [[NSMutableArray alloc] init];
    for(PFObject* vidgeo in self.vidgeoList)
    {
        // currently anything with a rating is displayed
        if (vidgeo[kParseRateUp])
        {
            [popularList addObject:vidgeo];
        }
    }
    [self dropPins:popularList];
}

-(void)filterByRecency
{
    NSMutableArray *recentList= [[NSMutableArray alloc] init];
    for(PFObject* vidgeo in self.vidgeoList){
        // currently anything within the last 4 days
        //this shoudl be chosen by the user
        if ([vidgeo.createdAt timeIntervalSinceNow]  > -recencyTimeInterval)
        {
            [recentList addObject:vidgeo];
        }
    }
    [self dropPins:recentList];
}

#pragma mark -  MoviePlayer methods
// Movie Playback Finished
- (void)moviePlayBackDidFinish:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

// Take Video Pressed
- (IBAction)takeVideo:(id)sender
{
    if (![PFUser currentUser]) {
        [self showLoginScreen];
    }else {
        [User updateVersion:[PFUser currentUser]];
    }
    [self.activityIndicator startAnimating];
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        if (!error) {
            currentLocation = geoPoint;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicator stopAnimating];
                [self performSegueWithIdentifier:@"recordVidgeo" sender:self];
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicator stopAnimating];
                [[[UIAlertView alloc] initWithTitle:@"Unkown Location" message:@"Unable to determine your location. Please check your location services and try again" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil] show];
            });
        }
    }];
    
}

// Prepare for Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showVidgeo"]) {
        [segue.destinationViewController setVidgeoObject:selectedVidgeo];
        [segue.destinationViewController setVideoURL:self.movieURL];
        [segue.destinationViewController setVidgeoData:vidgeoData];
    }
    if ([segue.identifier isEqualToString:@"recordVidgeo"]) {
        [segue.destinationViewController setCurrentLocation:currentLocation];
        [segue.destinationViewController setType:kVideoRecord];
    }
}

// Handle Memory Warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end