//
//  Constant.m
//  Vidgeo
//
//  Created by John Mercouris on 3/3/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "Constant.h"

@implementation Constant

int const kDailyVidgeoReward = 5;
int const kShareVidgeoReward = 2;
int const kUnlockTime = 86400;      //Once a day
double const kVersion = 1.2;


@end