//
//  User.m
//  Vidgeo
//
//  Created by John Mercouris on 3/3/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "User.h"
#import "Constant.h"


@implementation User

static User *shared = NULL;
int globalViews;
NSMutableArray* videoHistory;

- (id)init
{
    if ( self = [super init] )
    {
        globalViews = 0;
        videoHistory = [[NSMutableArray alloc]init];
        
    }
    return self;
}

+ (User *)sharedUser
{
    @synchronized(shared)
    {
        if ( !shared || shared == NULL )
        {
            // allocate the shared instance, because it hasn't been done yet
            shared = [[User alloc] init];
        }
        return shared;
    }
}

- (int) getGlobalViews
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"kGlobalViews"] intValue];
}

- (void) setGlobalViews:(int)inputGlobalViews
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:inputGlobalViews] forKey:@"kGlobalViews"];
    [defaults synchronize];
}

- (void) setLastOpenDate:(NSDate*) inputDate
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:inputDate forKey:@"kLastOpenDate"];
    [defaults synchronize];
}

- (NSDate*) getLastOpenDate
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"kLastOpenDate"];
}

-(void) addHistoryItem:(id) item
{
    if([videoHistory count] > 100)
    {
        [videoHistory removeLastObject];
    }
    [videoHistory insertObject:item atIndex:0];
}

-(NSMutableArray*) getHistory
{
    return videoHistory;
}

+ (void)updateVersion:(PFUser*)user{
    if ([user[kVidgeoVersion] doubleValue] != kVersion) {
        user[kVidgeoVersion] = [NSNumber numberWithDouble:kVersion];
        [user saveInBackground];
    }
    
}


@end