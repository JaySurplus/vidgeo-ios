//
//  AppDelegate.h
//  Vidgeo
//
//  Created by John Mercouris on 10/26/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
