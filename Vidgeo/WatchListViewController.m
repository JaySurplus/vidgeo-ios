//
//  WatchListViewController.m
//  Vidgeo
//
//  Created by John Mercouris on 2/24/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "WatchListViewController.h"
#import "ParseStore.h"
#import "vidgeoCell.h"

@interface WatchListViewController ()

@end

@implementation WatchListViewController
@synthesize watchListCollectionView;
@synthesize vidgeos;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.vidgeos = [[NSArray alloc] init];
    user = [PFUser currentUser];
    // Only use this code if you are already running it in a background
    // thread, or for testing purposes!
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [watchListCollectionView setDataSource:self];
    [watchListCollectionView setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 5;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 100;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    vidgeoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    //cell.vidgeoThumbnail = xyz
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

@end
