//
//  DrivingDirections.m
//  Vidgeo
//
//  Created by John Mercouris on 3/6/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "DrivingDirections.h"

@implementation DrivingDirections

+(void)openDirectionsWithLatitude: (float) latitude longitude: (float) longitude
{
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude,longitude);
    // Create MKMapItem out of coordinates
    MKPlacemark* placeMark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
    MKMapItem* destination =  [[MKMapItem alloc] initWithPlacemark:placeMark];
    
    // Using iOS6 native maps app
    [destination openInMapsWithLaunchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving}];
}
@end
