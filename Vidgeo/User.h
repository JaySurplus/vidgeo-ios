//
//  User.h
//  Vidgeo
//
//  Created by John Mercouris on 3/3/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParseStore.h"


@interface User : NSObject 
{
}

+(User*) sharedUser;
- (int) getGlobalViews;
- (void) setGlobalViews:(int)inputGlobalViews;
- (void) addHistoryItem:(id) item;
- (NSMutableArray*) getHistory;
- (void) setLastOpenDate:(NSDate*) date;
- (NSDate*) getLastOpenDate;
+ (void)updateVersion:(PFUser*)user;

@end
