//
//  AppDelegate.m
//  Vidgeo
//
//  Created by John Mercouris on 10/26/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import "AppDelegate.h"
#import "ParseStore.h"
#import "User.h"
#import "Constant.h"
#import "PublicVidgeoViewController.h"
#import "Reachability.h"

@implementation AppDelegate
UIActivityIndicatorView *activityIndicator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [Parse setApplicationId:@"xXWiN5md3HGI81eppzEa4I0N4O4YCyChY0MddlAD"
                  clientKey:@"6fAIhJNzLUX0JjeS2heDMOCvAeDfAgd056VL8eRt"];
    
    PFACL *defaultACL = [PFACL ACL];
    [defaultACL setPublicReadAccess:YES];
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
    //to track when the application is opened
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    [PFFacebookUtils initializeFacebook];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    // Reset badges when user opens app
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //check network reachability
    NetworkStatus networkStatus = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Internet Connection", nil)
                                                        message:NSLocalizedString(@"Please make sure you have a Wi-Fi or Cellular connection and try again.", nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:[PFFacebookUtils session]
                    fallbackHandler:^(FBAppCall *call) {
                        // If there is an active session
                        if (FBSession.activeSession.isOpen) {
                            // Check the incoming link
                            [self handleAppLinkData:call.appLinkData];
                        } else if (call.accessTokenData) {
                            NSLog(@"No active facebook session :( need to do something here");
                            if (![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]] ||
                                [[[PFFacebookUtils session] permissions] indexOfObject:@"user_actions:vidgeoapp"]== NSNotFound ||
                                [[[PFFacebookUtils session] permissions] indexOfObject:@"basic_info"]== NSNotFound){
                                [PFFacebookUtils linkUser:[PFUser currentUser]
                                              permissions:@[@"basic_info",@"publish_actions",@"user_actions:vidgeoapp"]
                                                    block:^(BOOL succeeded, NSError *error) {
                                                        if (succeeded) {
                                                            [self handleAppLinkData:call.appLinkData];
                                                        }else{
                                                            NSLog(@"Error linking with FB user %@",error);
                                                        }
                                                    }];
                            }
                        }
                    }];
}


#pragma mark - handling facebook vidgeo calls

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

- (void) handleAppLinkData:(FBAppLinkData *)appLinkData {
    NSString *targetURLString = appLinkData.originalQueryParameters[@"target_url"];
    if (targetURLString) {
        NSURL *targetURL = [NSURL URLWithString:targetURLString];
        NSDictionary *targetParams = [self parseURLParams:[targetURL query]];
        NSString *ref = [targetParams valueForKey:@"ref"];
        // Check for the ref parameter to check if this is one of
        // our incoming news feed link, otherwise it can be an
        // an attribution link
        if ([ref isEqualToString:@"notif"]) {
            // Get the request id
            NSString *requestIDParam = targetParams[@"request_ids"];
            NSArray *requestIDs = [requestIDParam
                                   componentsSeparatedByString:@","];
            
            // Get the request data from a Graph API call to the
            // request id endpoint
            if ([[[PFFacebookUtils session] permissions] indexOfObject:@"user_actions:vidgeoapp"]== NSNotFound ||
                [[[PFFacebookUtils session] permissions] indexOfObject:@"basic_info"]== NSNotFound) {
                [PFFacebookUtils linkUser:[PFUser currentUser]
                              permissions:@[@"basic_info",@"publish_actions",@"user_actions:vidgeoapp"]
                                    block:^(BOOL succeeded, NSError *error) {
                                        if (succeeded) {
                                            [self notificationGet:requestIDs[0]];
                                        }else{
                                            NSLog(@"Error linking with FB user %@",error);
                                        }
                                    }];
            }else{
                NSLog(@"We've got all the necessary permissions, make the request");
#warning handle multiple vidgeos being received at once
                [self notificationGet:requestIDs[0]];
            }
            
        }
    }
}

- (void) notificationGet:(NSString *)requestid {
    [self showSpinningWheel];
    NSLog(@"Making the graph request");
    [FBRequestConnection startWithGraphPath:requestid
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (!error) {
                                  //if we want to show a pop saying they have got a new one to watch
#pragma note add to watch list here
                                  //                                  [self displayVidgeoAlert:result];
                                  //currently we just display the vidgeo directly
                                  if (result[@"data"]) {
                                      NSString *jsonString = result[@"data"];
                                      NSData *jsonData = [jsonString
                                                          dataUsingEncoding:NSUTF8StringEncoding];
                                      if (!jsonData) {
                                          NSLog(@"JSON decode error: %@", error);
                                          return;
                                      }
                                      NSError *jsonError = nil;
                                      NSDictionary *requestData =
                                      [NSJSONSerialization JSONObjectWithData:jsonData
                                                                      options:0
                                                                        error:&jsonError];
                                      if (jsonError) {
                                          NSLog(@"JSON decode error: %@", error);
                                          return;
                                      }
                                      NSLog(@"YOu're about to see a vidgeo from a friend");
                                      [self displayVidgeo:requestData[kParseVidgeoID]];
                                  }
                                  // Delete the request notification
                                  [self notificationClear:result[@"id"]];
                              }else{
                                  NSLog(@"There was some error with the fB vidgeo request : %@",error);
                                  [self notificationClear:requestid];
                              }
                          }];
}

- (void) notificationClear:(NSString *)requestid {
    // Delete the request notification
    [FBRequestConnection startWithGraphPath:requestid
                                 parameters:nil
                                 HTTPMethod:@"DELETE"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (!error) {
                                  NSLog(@"Request deleted");
                              }
                          }];
}
-(void)displayVidgeoAlert:(id)result{
    [self removeSpinningWheel];
    NSString *title;
    NSString *message;
    NSError *error;
    if (result[@"data"]) {
        title = [NSString
                 stringWithFormat:@"%@ sent you a Vidgeo",
                 result[@"from"][@"name"]];
        NSString *jsonString = result[@"data"];
        NSData *jsonData = [jsonString
                            dataUsingEncoding:NSUTF8StringEncoding];
        if (!jsonData) {
            NSLog(@"JSON decode error: %@", error);
            return;
        }
        NSError *jsonError = nil;
        NSDictionary *requestData =
        [NSJSONSerialization JSONObjectWithData:jsonData
                                        options:0
                                          error:&jsonError];
        if (jsonError) {
            NSLog(@"JSON decode error: %@", error);
            return;
        }
        message =
        [NSString stringWithFormat:@"VidgeoID: %@, Karma: %@",
         requestData[kParseVidgeoID],
         requestData[@"social_karma"]];
    } else {
        title = [NSString
                 stringWithFormat:@"%@ sent you a request",
                 result[@"from"][@"name"]];
        message = result[@"message"];
    }
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:title
                          message:message
                          delegate:nil
                          cancelButtonTitle:@"Watch it"
                          otherButtonTitles:nil,
                          nil];
    [alert show];
    
}
-(void)displayVidgeo:(NSString*)vidgeoID{
    [PFCloud callFunctionInBackground:@"videoForVidgeo"
                       withParameters:@{@"vidgeo": vidgeoID}
                                block:^(PFObject *videoObject, NSError *error) {
                                    if (error) {
                                        [self displayVidgeoError:error];
                                        return;
                                    } else if(videoObject) {
                                        NSData * vidgeoData =[(PFFile*) videoObject[kParseVideoData] getData];
                                        NSURL * movieURL = [ParseStore NSURLfromNSData:vidgeoData];
                                        //Play the video on main thread
                                        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                                            [self removeSpinningWheel];
                                            // perform the segue
                                            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                            PublicVidgeoViewController * showVidgeoVC = (PublicVidgeoViewController *)[sb instantiateViewControllerWithIdentifier:@"publicVidgeoVC"];
                                            [showVidgeoVC setVideoURL:movieURL];
                                            [showVidgeoVC setVidgeoData:vidgeoData];
                                            [showVidgeoVC setVidgeoObject:videoObject];
                                            [[self topMostController] presentViewController:showVidgeoVC
                                                                                   animated:YES
                                                                                 completion:nil];
                                            
                                        }];
                                    } else {
                                        [self displayVidgeoError:error];
                                        NSLog(@"Failed to fetch the vidgeo locations!");
                                        return ;
                                    }
                                }];
}

#pragma mark - activity Indicator helper methods
-(void)showSpinningWheel
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    CGRect bounds = [[UIApplication sharedApplication] keyWindow].bounds;
    activityIndicator.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));;
    activityIndicator.hidesWhenStopped = YES;
    [[[UIApplication sharedApplication] keyWindow] addSubview:activityIndicator];
    [activityIndicator startAnimating];
}
-(void)removeSpinningWheel
{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
}
-(void)displayVidgeoError:(NSError*)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self removeSpinningWheel];
        NSLog(@"Couldn't load the video :(");
        NSLog(@"%@", error);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[error userInfo] objectForKey:@"error"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
    });
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


@end
