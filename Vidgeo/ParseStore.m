//
//  ParseStore.m
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 10/26/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import "ParseStore.h"
#import "NSString+MD5.h"
#import <Security/Security.h>

@interface ParseStore()

@end

@implementation ParseStore
@synthesize UUID;
NSDateFormatter *dateFormatter;

#pragma mark  - Vidgeo modification methods
+(void)rateVidgeoUp:(PFObject*)vidgeoID{
    [PFCloud callFunctionInBackground:@"rateUp"
                       withParameters:@{@"vidgeoID": vidgeoID.objectId, @"userId":[PFUser currentUser].objectId}
                                block:^(id success, NSError *error) {
                                    if (error) {
                                        NSLog(@"Unable update the vidgeo rating");
                                        NSLog(@"%@", error);
                                        [[[UIAlertView alloc]
                                          initWithTitle:[[error userInfo] objectForKey:@"error"]
                                          message:nil delegate:self
                                          cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
                                        return;
                                    }else if(success){
                                        //                                        NSLog(@"Successfully saved vidgeo rating!!");
                                    }else{
                                        NSLog(@"Failed to save vidgeo rating!");
                                    }
                                }];
}

+(void)rateVidgeoDown:(PFObject*)vidgeoID {
    [PFCloud callFunctionInBackground:@"rateDown"
                       withParameters:@{@"vidgeoID": vidgeoID.objectId, @"userId":[PFUser currentUser].objectId}
                                block:^(id success, NSError *error) {
                                    if (error) {
                                        NSLog(@"Unable update the vidgeo rating");
                                        NSLog(@"%@", error);
                                        [[[UIAlertView alloc]
                                          initWithTitle:[[error userInfo] objectForKey:@"error"]
                                          message:nil delegate:self
                                          cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
                                        return;
                                    }else if(success){
//                                      NSLog(@"Successfully saved vidgeo rating!!");
                                    }else{
                                        NSLog(@"Failed to save vidgeo rating!");
                                    }
                                }];
}
+(void)flagVidgeo:(PFObject*)vidgeo {
    [PFCloud callFunctionInBackground:@"flagVidgeo"
                       withParameters:@{@"vidgeo": vidgeo.objectId}
                                block:^(id success, NSError *error) {
                                    if (error) {
                                        NSLog(@"Unable flag the vidgeo");
                                        NSLog(@"%@", error);
                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[error userInfo] objectForKey:@"error"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                        [alertView show];
                                        return;
                                    }else if(success){
                                        return;
                                    }else{
                                        NSLog(@"Failed to flagged vidgeo!");
                                    }
                                }];
}
//delete a Vidgeo, it's vidgeo data and the thumbnail
+(void)deleteMyVidgeo:(PFObject*)myVidgeo {
    [PFCloud callFunctionInBackground:@"deleteVidgeo"
                       withParameters:@{@"vidgeo": myVidgeo.objectId}
                                block:^(id success, NSError *error) {
                                    if (error) {
                                        NSLog(@"Unable to delete the Vidgeo ");
                                        return;
                                    }else if(success){
                                        return;
                                    }else{
                                        NSLog(@"Failed to delete vidgeo !");
                                    }
                                }];
    
}

+(void)updateViewCount:(PFObject*)vidgeo {
    [PFCloud callFunctionInBackground:@"updateViewCount"
                       withParameters:@{@"vidgeoID": vidgeo.objectId}
                                block:^(id success, NSError *error) {
                                    if (error) {
                                        NSLog(@"Unable to update view count the Vidgeo : %@",error);
                                        return;
                                    }else if(success){
                                        //                                        NSLog(@"Successfully saved vidgeo viewCount!!");
                                    }else{
                                        NSLog(@"Failed to save vidgeo view count!");
                                    }
                                }];
}

+(NSString*)encryptPhrase:(NSString*)phrase{
    NSString *private = @"CzyJ2qMMW1DyQ7ol2VTXB3lhwNFEyPEqQHFrY7yIpIA";
    NSString *md5String = [NSString stringWithFormat:@"%@%@", phrase, private];
    return [md5String MD5];
}

+(void)lockVidgeo:(PFObject*)vidgeo withPasscode:(NSString*)phrase {
    NSLog(@"Encrypted phrase: %@",phrase);
    //encrypt the phrase here
    NSString *encryptedPhrase = [self encryptPhrase:phrase];
    [PFCloud callFunctionInBackground:@"lockUserVidgeo"
                       withParameters:@{@"vidgeo": vidgeo.objectId, @"passphrase": encryptedPhrase}
                                block:^(id success, NSError *error) {
                                    if (error) {
                                        NSLog(@"Unable to lock the Vidgeo : %@",error);
                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error locking your Vidgeo" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                        [alertView show];
                                        return;
                                    }else if(success){
                                        //                                        NSLog(@"Successfully saved locked vidgeo!!");
                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Your Vidgeo has been locked" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                        [alertView show];
                                    }else{
                                        NSLog(@"Failed to save vidgeo view count!");
                                    }
                                }];
}

#pragma mark -
#pragma mark helper methods

//write NSData to system memory & return address

+(NSURL *)NSURLfromNSData:(NSData*)videoData {
    NSLog(@"Converting NSData -> videoURL");
    NSString *urlString = [NSTemporaryDirectory() stringByAppendingPathComponent:@"vidgeo.mov"];
    NSURL *url = [NSURL fileURLWithPath:urlString];
    
    [videoData writeToURL:url atomically:NO];
    return url;
}

+(NSString*)stringFromDate:(NSDate*)vidgeoDate ofType:(NSString*)type{
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:type];
    return [dateFormatter stringFromDate:vidgeoDate];
}


+(NSMutableArray*)getFriends{
    PFUser *user =[PFUser currentUser];
    return [(NSArray*)[user relationforKey:kParseFriends] mutableCopy];
}
+(void)addFriend:(NSString*)fbID{
    PFUser *user =[PFUser currentUser];
    if (!user[kParseFriends]) {
        user[kParseFriends]= @[fbID];
    }
    [user[kParseFriends] addObject:fbID];
    [user saveInBackground];
    
}

+ (NSString *)getUUID {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (NSString *)CFBridgingRelease(string) ;
}

@end
