//
//  HistoryViewController.h
//  Vidgeo
//
//  Created by John Mercouris on 3/3/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *historyCollectionView;
@property (strong, nonatomic)  NSURL *movieURL;


@end
