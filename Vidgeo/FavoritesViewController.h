//
//  FavoritesViewController.h
//  Vidgeo
//
//  Created by John Mercouris on 2/24/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParseStore.h"

@interface FavoritesViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,UIAlertViewDelegate>
{
    PFUser* user;
}
@property (strong, nonatomic) IBOutlet UICollectionView *favoritesCollectionView;
@property (nonatomic, strong) NSArray *vidgeos;
@property (strong, nonatomic)  NSURL *movieURL;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
