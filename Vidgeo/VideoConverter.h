//
//  VideoConverter.h
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 3/28/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoConverter : NSObject

+(NSURL*)movToMp4:(NSURL*)videoURL;
@end
