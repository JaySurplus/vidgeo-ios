//
//  SettingsViewController.m
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 12/5/13.
//  Copyright (c) 2013 John Mercouris. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()
@end

@implementation SettingsViewController
int numberOfRows = 2;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    user = [PFUser currentUser];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section)
    {
        case 0:
            return ([PFUser currentUser]) ? 4 : 1;
        case 1:
            return 1; //numberOfRows; for future implementation
        default:
            return 0;
    }
}

- (IBAction)logOut:(id)sender
{
    [PFUser logOut];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        switch (indexPath.row) {
            case 0:
                [cell.textLabel  setText:(user==nil) ? @"Not Logged In" : @"Account Information"];
                break;
            case 1:
                [cell.detailTextLabel setText:(user!=nil) ? user.username : @"Default"];
                break;
            case 2:
                [cell.detailTextLabel setText:(user!=nil) ? user.email : @"default@vidgeo.com"];
                break;
        }
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
                [cell.textLabel  setText:@"User Agreement"];
                break;
                
                //for adding other settings in the future
        }
    }
    
    return cell;
}

#pragma tableview Delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 3) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Reset"
                                                          message:@"Are you sure you want to reset your password?"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Yes", nil];
        
        [message show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if (buttonIndex != [alertView cancelButtonIndex])
    {
        //User clicked ok
        NSLog(@"Ok, they don't want to reset password");
    }
    else if([title isEqualToString:@"Yes"])
    {
        //User clicked cancel
        NSLog(@"Let's reset their password!");
        [PFUser requestPasswordResetForEmailInBackground: user.email];
        [[[UIAlertView alloc] initWithTitle:@"Reset Complete" message:@"Instructions have been sent to your email address." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
    }
}
@end
