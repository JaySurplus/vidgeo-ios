//
//  SharingViewController.m
//  Vidgeo
//
//  Created by Eyoel Berhane Asfaw on 3/7/14.
//  Copyright (c) 2014 John Mercouris. All rights reserved.
//

#import "SharingViewController.h"
#import "User.h"
#import "Constant.h"

@interface SharingViewController ()

@end

@implementation SharingViewController
@synthesize popControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)shareLocation:(PFObject *)vidgeoInfo{
    //Share your location information
    //http://stackoverflow.com/questions/20064781/how-to-share-my-current-location-to-uiactivityviewcontroller
    PFGeoPoint *geoPoint = vidgeoInfo[kParseLocation];
    // Insert Coordinates into vcard for "PIN" recognition, see sample pin files
    //NSString *coordinates = [NSString stringWithFormat:@"http://maps.apple.com/maps?q=%f,%f",  geoPoint.latitude, geoPoint.longitude];
    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:geoPoint.latitude longitude:geoPoint.longitude];
    CLGeocoder *geocoder;
    geocoder = [[CLGeocoder alloc]init];
    
    [geocoder reverseGeocodeLocation:userLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         CLPlacemark *rootPlacemark = placemarks[0];
         MKPlacemark *evolvedPlacemark = [[MKPlacemark alloc]initWithPlacemark:rootPlacemark];
         
         ABRecordRef persona = ABPersonCreate();
         ABRecordSetValue(persona, kABPersonFirstNameProperty, (__bridge CFTypeRef)(evolvedPlacemark.name), nil);
         ABMutableMultiValueRef multiHome = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
         
         bool didAddHome = ABMultiValueAddValueAndLabel(multiHome, (__bridge CFTypeRef)(evolvedPlacemark.addressDictionary), kABHomeLabel, NULL);
         
         if(didAddHome)
         {
             ABRecordSetValue(persona, kABPersonAddressProperty, multiHome, NULL);
             
             NSLog(@"Address saved.");
         }
         
         NSArray *individual = [[NSArray alloc]initWithObjects:(__bridge id)(persona), nil];
         CFArrayRef arrayRef = (__bridge CFArrayRef)individual;
         NSData *vcards = (__bridge NSData *)ABPersonCreateVCardRepresentationWithPeople(arrayRef);
         
         NSString* vcardString;
         vcardString = [[NSString alloc] initWithData:vcards encoding:NSASCIIStringEncoding];
         NSLog(@"%@",vcardString);
         
         
         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
         NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
         
         NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"pin.loc.vcf"];
         [vcardString writeToFile:filePath
                       atomically:YES encoding:NSUTF8StringEncoding error:&error];
         
         NSURL *url =  [NSURL fileURLWithPath:filePath];
         NSLog(@"url> %@ ", [url absoluteString]);
         
         
         // Share Code //
         NSArray *itemsToShare = [[NSArray alloc] initWithObjects: url, nil] ;
         UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
         activityVC.excludedActivityTypes = @[UIActivityTypePrint,
                                              UIActivityTypeCopyToPasteboard,
                                              UIActivityTypeAssignToContact,
                                              UIActivityTypeSaveToCameraRoll,
                                              UIActivityTypePostToWeibo];
         
         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
         {
             [self presentViewController:activityVC animated:YES completion:nil];
         }else
         {
             popControl = [[UIPopoverController alloc] initWithContentViewController:activityVC];
         }
         
     }];
}


#pragma mark - facebook share stuff

-(void)postOnWall:(PFObject *)vidgeoInfo{
    if (![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]] ||
        [[[PFFacebookUtils session] permissions] indexOfObject:@"publish_actions"]== NSNotFound) {
        [PFFacebookUtils linkUser:[PFUser currentUser] permissions:@[@"basic_info",@"publish_actions",@"user_actions:vidgeoapp"] block:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                NSLog(@"Woohoo, user logged in with Facebook!");
                // save thier user info in PF Object for later use
                if (![PFUser currentUser][@"fbId"]) {
                    FBRequest *request = [FBRequest requestForMe];
                    
                    // Send request to Facebook
                    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                        if (!error) {
                            // result is a dictionary with the user's Facebook data
                            NSDictionary *userData = (NSDictionary *)result;
                            NSString *facebookID = userData[@"id"];
                            PFUser* me= [PFUser currentUser];
                            me[@"name"]=userData[@"name"];
                            NSLog(@"Got the facebook ID: %@", facebookID);
                            me[@"fbId"]=facebookID;
                            [me saveInBackground];
                            
                        }
                    }];
                }
                [self postOnWall:vidgeoInfo];
            }if (error) {
                NSLog(@"The error with connecting to facebook: %@",error);
            }
        }];
    }else{
        NSString* vidgeoURL = [NSString stringWithFormat:@"http://vidgeo.co/?vidgeoID=%@",vidgeoInfo.objectId];
        //ok let's post on your wall now
        UIImage *shareImage;

        if (vidgeoInfo[kParseThumbnail]) {
            PFFile *theImage = vidgeoInfo[kParseThumbnail];
            NSData *imageData = [theImage getData];
            UIImage *image = [UIImage imageWithData:imageData];
            shareImage = [[UIImage alloc] initWithCGImage: image.CGImage
                                                                scale: 1.0
                                                          orientation: UIImageOrientationRight];
        } else {
            shareImage =[UIImage imageNamed:@"iTunesArtwork.png"];
        }
//        BOOL canShare = [FBDialogs canPresentShareDialogWithPhotos];
//        if (canShare) {
////            Posting a thumbnail of the vidgeo
//            FBPhotoParams *params = [[FBPhotoParams alloc] init];
//            params.photos = @[shareImage];
//            
//            [FBDialogs presentShareDialogWithPhotoParams:params
//                                             clientState:nil
//                                                 handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
//                                                     if (error) {
//                                                         NSLog(@"Error: %@", error.description);
//                                                     } else {
//                                                         NSLog(@"Success!");
//                                                     }
//                                                 }];
//        } else {
//            Psoting a link to the vidgeo
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           @"I watched a Vidgeo!", @"name",
                                           @"Watching this Vidgeo!", @"caption",
                                           @"I watched this cool video, check it out", @"description",
                                           vidgeoURL, @"link",
                                           @"http://vidgeo.co/images/vidgeoapp.jpg", @"picture",
                                           nil];
            
            // Make the request
            [FBRequestConnection startWithGraphPath:@"/me/feed"
                                         parameters:params
                                         HTTPMethod:@"POST"
                                  completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                      if (!error) {
                                          // Link posted successfully to Facebook
                                          NSLog(@"result: %@", result);
                                      } else {
                                          // An error occurred, we need to handle the error
                                          // See: https://developers.facebook.com/docs/ios/errors
                                          NSLog(@"%@", error.description);
                                      }
                                  }];
            
        }
        
        
//    }
    
}

- (void)sendRequest:(PFObject*) vidgeoInfo {
    
    
    if (![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]] ||
        [[[PFFacebookUtils session] permissions] indexOfObject:@"basic_info"]== NSNotFound) {
        [PFFacebookUtils linkUser:[PFUser currentUser] permissions:@[@"basic_info",@"publish_actions"] block:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                NSLog(@"Woohoo, user logged in with Facebook!");
                // save thier user info in PF Object for later use
                if (![PFUser currentUser][@"fbId"]) {
                    FBRequest *request = [FBRequest requestForMe];
                    
                    // Send request to Facebook
                    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                        if (!error) {
                            // result is a dictionary with the user's Facebook data
                            NSDictionary *userData = (NSDictionary *)result;
                            NSString *facebookID = userData[@"id"];
                            PFUser* me= [PFUser currentUser];
                            me[@"name"]=userData[@"name"];
                            NSLog(@"Got the facebook ID: %@", facebookID);
                            me[@"fbId"]=facebookID;
                            [me saveInBackground];
                            
                        }
                    }];
                }
                [self sendRequest:vidgeoInfo];
            }if (error) {
                NSLog(@"The error with connecting to facebook: %@",error);
            }
        }];
    }else{
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization
                            dataWithJSONObject:@{
                                                 kParseVidgeoID: vidgeoInfo.objectId,
                                                 kShareVidgeo: @"1"}
                            options:0
                            error:&error];
        if (!jsonData) {
            NSLog(@"JSON conversion error before share: %@", error);
            return;
        }
        
        NSString *vidgeoStr = [[NSString alloc]
                               initWithData:jsonData
                               encoding:NSUTF8StringEncoding];
        
        NSMutableDictionary* params = [@{@"data" : vidgeoStr} mutableCopy];
        
        NSLog(@"Facebook connected and about to share with friends");
        
        // Display the requests dialog
        [FBWebDialogs
         presentRequestsDialogModallyWithSession:nil
         message:@"Watch this awesome video"
         title:@"Watch Vidgeos"
         parameters:params
         handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
             if (error) {
                 // Error launching the dialog or sending the request.
                 NSLog(@"Error sending request.");
             } else {
                 if (result == FBWebDialogResultDialogNotCompleted) {
                     // User clicked the "x" icon
                     NSLog(@"User canceled request.");
                 } else {
                     // Handle the send request callback
                     NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                     if (![urlParams valueForKey:@"request"]) {
                         // User clicked the Cancel button
                         NSLog(@"User canceled request.");
                     } else {
                         // User clicked the Send button
                         NSString *requestID = [urlParams valueForKey:@"request"];
                         NSLog(@"Request ID: %@", requestID);
                         User *singletonInstanceUser = [User sharedUser];
                         [singletonInstanceUser setGlobalViews:([singletonInstanceUser getGlobalViews] + kShareVidgeoReward)];
                     }
                 }
             }
         }];
    }
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}



#pragma mark - PFLogInViewControllerDelegate
-(void)showLoginScreen
{
    MyLogInViewController *logInViewController = [[MyLogInViewController alloc] init];
    [logInViewController setDelegate:self]; // Set ourselves as the delegate
    
    MySignUpViewController *signUpViewController = [[MySignUpViewController alloc] init];
    [signUpViewController setDelegate:self]; // Set ourselves as the delegate
    
    [logInViewController setSignUpController:signUpViewController];
    
    [self presentViewController:logInViewController animated:YES completion:NULL];
}

// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length && password.length) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil)
                                message:NSLocalizedString(@"Make sure you fill out all of the information!", nil)
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [logInController dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [self.navigationController popViewControllerAnimated:YES];
}





@end