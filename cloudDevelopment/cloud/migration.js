// ----- DB MIGRATION FUNCTIONS ---------

Parse.Cloud.job("videoMigration", function(request, status) {
	// Set up to modify user data
	Parse.Cloud.useMasterKey();
	var count = 0;
	// Query for all videos and locations
	var vidgeoLocation = Parse.Object.extend("VidgeoLocation");
	var locQuery = new Parse.Query(vidgeoLocation);
	var vidgeoStore = Parse.Object.extend("VidgeoStore");
	var vidQuery = new Parse.Query(vidgeoStore);
	vidQuery.limit(200);
	vidQuery.descending("createdAt");
	
	vidQuery.find({
    success: function(videos) {
      for (var i = 0; i < videos.length; ++i) {
      	video = videos[i];
        locQuery.equalTo("videoID", video.id);
		locQuery.find({
			success: function(vidgeos) {
			// The object was retrieved successfully.
				var vidgeo = null;
				if(vidgeos.length >=1){
					vidgeo = vidgeos[0];
					var videoData = vidgeo.get("videoData");
					if(!videoData){	// if not previously set already
						count++;
						vidgeo.set("videoData",video.get("videoData"));
						vidgeo.save();				// attach the video file to vidgeo-loc object
					}
				}
			},
			error: function(object, error) {
				response.error("Currently Unable to fetch Vidgeo loc obj and set it's video");
			}
		});
      }
      console.log("Successfully migrated: "+count+" videos");
//       status.success("Migration completed successfully.");
    
    },
    error: function() {
      response.error("Uh oh, something went wrong with the migration.");
    }
  });
});


Parse.Cloud.job("thumbnailMigration", function(request, status) {
	// Set up to modify user data
	Parse.Cloud.useMasterKey();
	var count = 0;
	// Query for all videos and locations
	var vidgeoLocation = Parse.Object.extend("VidgeoLocation");
	var locQuery = new Parse.Query(vidgeoLocation);
	var vidgeoStore = Parse.Object.extend("ThumbnailStore");
	var vidQuery = new Parse.Query(vidgeoStore);
	vidQuery.limit(200);
	vidQuery.descending("createdAt");
	
	vidQuery.find({
    success: function(videos) {
      for (var i = 0; i < videos.length; ++i) {
      	video = videos[i];
        locQuery.equalTo("videoID", video.get("videoID"));
		locQuery.find({
			success: function(vidgeos) {
			// The object was retrieved successfully.
				var vidgeo = null;
				if(vidgeos.length >=1){
					vidgeo = vidgeos[0];
					var videoData = vidgeo.get("thumbnail");
					if(!videoData){	// if not previously set already
						count++;
						vidgeo.set("thumbnail",video.get("videoData"));
						vidgeo.save();				// attach the video file to vidgeo-loc object
					}
				}
			},
			error: function(object, error) {
				response.error("Currently Unable to fetch Vidgeo loc obj and set it's thumbnail");
			}
		});
      }
      console.log("Successfully migrated: "+count+" videos");
//       status.success("Migration completed successfully.");
    
    },
    error: function() {
      response.error("Uh oh, something went wrong with the migration.");
    }
  });
});
// END OF MIGRATION CODE
// ---------------------------------------------------

