require('cloud/migration.js');
require('cloud/rootframework.js');
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});

Parse.Cloud.define("rateVidgeoUp", function(request, response) {
	Parse.Cloud.useMasterKey();
	var vidgeoLocation = Parse.Object.extend("VidgeoLocation");
	var query = new Parse.Query(vidgeoLocation);
	query.get(request.params.vidgeoID, {
	success: function(vidgeo) {
	// The object was retrieved successfully.
		var ratings = vidgeo.get("rateUp");
		 
		if(!ratings){
			ratings = [];
		}
		if(ratings.indexOf(request.params.userId) > -1){
		//if they have already rated it up
			response.error("User has already liked this vidgeo");
		}else{
			ratings.push(request.params.userId);
			vidgeo.set("rateUp", ratings);
			vidgeo.save(null, {
				success: function(vidgeo) {
					//have successfully updated the upVotes
					response.success("Rated vidgeo Down");
				}
			});
		}
		return;
	},
	error: function(object, error) {
	response.error("Currently Unable to upvote the selected Vidgeo");
	}
	});
});
Parse.Cloud.define("rateVidgeoDown", function(request, response) {
	Parse.Cloud.useMasterKey();
	var vidgeoLocation = Parse.Object.extend("VidgeoLocation");
	var query = new Parse.Query(vidgeoLocation);
	query.get(request.params.vidgeoID, {
	success: function(vidgeo) {
	// The object was retrieved successfully.
		var ratings = vidgeo.get("rateDown");
		if(!ratings){
			ratings = [];
		}
		if(ratings.indexOf(request.params.userId) > -1){
		//if they have already rated it up
			response.error("User has already liked this vidgeo");
		}else{
			ratings.push(request.params.userId);
			vidgeo.set("rateDown", ratings);
			vidgeo.save(null, {
				success: function(vidgeo) {
					//have successfully updated the upVotes
					response.success("Rated vidgeo Down");
				}, error: function(vidgeo, error) {
				response.error("Currently Unable to down vote the selected Vidgeo: "+error);
			}
			});
		}
	},
	error: function(object, error) {
	// error is a Parse.Error with an error code and description.
	response.error("Currently Unable to upvote the selected Vidgeo");
	}
	});
});

//can also record the user that viewed the video in the future
Parse.Cloud.define("updateViewCount", function(request, response) {
	Parse.Cloud.useMasterKey();
	var vidgeoLocation = Parse.Object.extend("VidgeoLocation");
	var query = new Parse.Query(vidgeoLocation);
	query.get(request.params.vidgeoID, {
	success: function(vidgeo) {
	// The object was retrieved successfully.
		var viewcount = vidgeo.get("viewCount");
		if(!viewcount){
			viewcount = 0;
		}
		viewcount+=1;
		vidgeo.set("viewCount", viewcount);
		vidgeo.save(null, {
  			success: function(vidgeo) {
  				//have successfully updated the upVotes
  				response.success();
  			},
  			error: function(vidgeo, error) {
				response.error("Currently Unable to update the view count the selected Vidgeo: "+error);
			}
		});
		
	},
	error: function(object, error) {
		response.error("Currently Unable to increment view count for the selected Vidgeo");
	}
	});
});

//creates an entry in the VidgeoLock table for locking a video file
Parse.Cloud.define("lockVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
	var vidgeoLocation = Parse.Object.extend("VidgeoLocation");
	var VidgeoLock = Parse.Object.extend("VidgeoLock");
	var query = new Parse.Query(vidgeoLocation);
	query.get(request.params.vidgeoID, {
	success: function(vidgeo) {
	// The object was retrieved successfully.
		var locked = vidgeo.get("lockedVidgeo");
		//only the creator can modify the password
		console.log("The request id "+vidgeo.get("user").id);
		if(vidgeo.get("user").id === request.user.id){
			console.log("this is the correct user trying to update the passphrase");
			if(locked){
				//we should update the new password
				//get VidgeoLock object and then update with new password
				/******* needs work ****/
				var query = new Parse.Query(VidgeoLock);
				query.get(locked, {
					success: function(vidgeoLock) {
						// The count request succeeded. Show the count
						vidgeoLock.set("passphrase",request.params.passphrase);
						vidgeoLock.save(null, {
						success: function(videoLock) {
								//have successfully updated the the vidgeoLock table
								response.success();
							},
							error: function(error) {
								response.error("Currently Unable to lock the selected Vidgeo: "+error);
							}
						});
					  },
					error: function(error) {
						// The request failed
						response.error("Currently Unable to lock the selected Vidgeo: "+error);
					  }
				});
			}//end of already locked loop
			else{
				
				var vidgeoLock = new VidgeoLock();
				vidgeoLock.set("vidgeoID", request.params.vidgeoID);
				vidgeoLock.set("videoID", vidgeo.get("videoID"));
				vidgeoLock.set("passphrase",request.params.passphrase);
				vidgeoLock.set("lockingUser", request.user.id);
				vidgeoLock.save(null, {
				success: function(videoLock) {
						//have successfully updated the the vidgeoLock table
						//clear out the videoID so that they can't access it directly
						vidgeo.set("videoID", "");
						console.log("vidgeoLock ID :" + vidgeoLock.id);
						vidgeo.set("lockedVidgeo", vidgeoLock.id);
						//update the vidgeo as locked
						vidgeo.save(null, {
							success: function(vidgeo) {
								//have successfully updated the upVotes
								response.success();
							},
							error: function(videoLock, error) {
								response.error("Currently Unable to lock the selected Vidgeo: "+error);
							}
						});
					},
					error: function(vidgeo, error) {
						response.error("Currently Unable to lock the selected Vidgeo: "+error);
					}
				});
			}//is not locked loop	
		}
	},
	error: function(object, error) {
		response.error("Currently Unable to lock the selected Vidgeo: "+error);
	}
	});
});


//need : Unlock Vidgeo
Parse.Cloud.define("unlockVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
	var VidgeoStore = Parse.Object.extend("VidgeoStore");
	var VidgeoLock = Parse.Object.extend("VidgeoLock");
	var query = new Parse.Query(VidgeoLock);
	query.get(request.params.vidgeoLockId, {
	success: function(vidgeoLock) {
	// The object was retrieved successfully.
	//compare the password
		if(vidgeoLock.get("passphrase") === request.params.passphrase){
		
			var videoId = vidgeoLock.get("videoID");
			var secondQuery = new Parse.Query(VidgeoStore);
			secondQuery.get(videoId, {
				success: function(vidgeoData) {
					//have successfully fetched the users videoData PFObject
					response.success(vidgeoData);
				},
				error: function(vidgeoData, error) {
					response.error("Currently unable to fetch the selected Vidgeo: "+error);
				}
			});
		}else{
			response.error("Incorrect password ");
		}
	},
	error: function(object, error) {
		response.error("Currently unable to fetch the selected vidgeo Lock: "+error);
	}
	});
});

//need to give creator of vidgeo access without password
Parse.Cloud.define("fetchMyLockedVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
	var VidgeoStore = Parse.Object.extend("VidgeoStore");
	var VidgeoLock = Parse.Object.extend("VidgeoLock");
	var query = new Parse.Query(VidgeoLock);
	query.get(request.params.vidgeoLockId, {
	success: function(vidgeoLock) {
	// The object was retrieved successfully.
		if(vidgeoLock.get("lockingUser") === request.user.id){
		
			var videoId = vidgeoLock.get("videoID");

			var secondQuery = new Parse.Query(VidgeoStore);
			secondQuery.get(videoId, {
				success: function(vidgeoData) {
					//have successfully fetched the users videoData PFObject
					response.success(vidgeoData);
				},
				error: function(vidgeoData, error) {
					response.error("Currently unable to fetch the selected Vidgeo: "+error);
				}
			});
		}else{
			response.error("Incorrect user permissions ");
		}
		
	},
	error: function(object, error) {
		response.error("Currently unable to fetch the selected vidgeo Lock: "+error);
	}
	});
});

//need to let the creator unlock their vidgeo
Parse.Cloud.define("unlockMyVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
	var VidgeoLocation = Parse.Object.extend("VidgeoLocation");
	var VidgeoLock = Parse.Object.extend("VidgeoLock");
	var query = new Parse.Query(VidgeoLock);
	query.get(request.params.vidgeoLockId, {
	success: function(vidgeoLock) {
	// The object was retrieved successfully.
		if(vidgeoLock.get("lockingUser") === request.user.id){
		
			var vidgeoId = vidgeoLock.get("vidgeoID");
			var videoId = vidgeoLock.get("videoID");
			var secondQuery = new Parse.Query(VidgeoLocation);
			secondQuery.get(vidgeoId, {
				success: function(vidgeo) {
					//have successfully fetched the users videoData PFObject
					vidgeo.unset("lockedVidgeo");
					vidgeo.set("videoID",videoId);
					vidgeo.save(null, {
							success: function(vidgeo) {
								//have successfully updated the vidgeoInfo
								//can now delete the vidgeoLock
								response.success(vidgeoLock.destroy());
							},
							error: function(videoLock, error) {
								response.error("Currently unable to remove vidgeo lock: "+error);
							}
						});
					response.success();
				},
				error: function(vidgeoData, error) {
					response.error("Currently unable to fetch the selected Vidgeo: "+error);
				}
			});
		}else{
			response.error("Incorrect user permissions ");
		}	
	},
	error: function(object, error) {
		response.error("Currently unable to fetch the selected vidgeo Lock: "+error);
	}
	});
});