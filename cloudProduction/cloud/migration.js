// ---------------------------------------------------
// ----- DB MIGRATION FUNCTIONS ----------------------
// ---------------------------------------------------

Parse.Cloud.job("videoMigration", function(request, status) {
	// Set up to modify user data
	Parse.Cloud.useMasterKey();
	// New Store Table
	var dataStore = Parse.Object.extend("dataStore");
	// Cursor Store Table
	var dataQuery = new Parse.Query(dataStore);
	// Original Video Table
	var vidgeoStore = Parse.Object.extend("VidgeoStore");
	// Cursor for Video Table
	var vidgeoQuery = new Parse.Query(vidgeoStore);
	// Limit Query
	vidgeoQuery.limit(1000);
	// Order by date
	vidgeoQuery.descending("createdAt");
	
	vidgeoQuery.find({
		//-------------------------------------------------------
		// Find Success
		success: function(videos) {
			for (var i = 0; i < videos.length; ++i) {
				var video = videos[i];
				(function(video) {
					// SQL Like
					dataQuery.equalTo("videoID", video.id);
					// Retrieve Like Video
					dataQuery.find({
						success: function(vidgeos) {
							// The object was retrieved successfully.
							var vidgeo;
							if(vidgeos.length >= 1) {
								vidgeo = vidgeos[0];
								// If not previously set already
								vidgeo.set("videoData",video.get("videoData"));
								// Attach the video file to vidgeo-loc object
								vidgeo.save();
								//if(vidgeo.id === "6oBWtYnJN0") {
								//	status.success("Success; exiting cleanly.");
								//}
							}
						},
						error: function(object, error) {
							response.error("Currently Unable to fetch Vidgeo loc obj and set it's video");
						}
					});
				}(video));}
			},
			//-------------------------------------------------------
			// Find Error
			error: function() {
				response.error("Error with migration.");
			}
		});
	});

	Parse.Cloud.job("thumbnailMigration", function(request, status) {
		// Set up to modify user data
		Parse.Cloud.useMasterKey();
		// New Store Table
		var dataStore = Parse.Object.extend("dataStore");
		// Cursor Store Table
		var dataQuery = new Parse.Query(dataStore);
		// Original Video Table
		var vidgeoStore = Parse.Object.extend("ThumbnailStore");
		// Cursor for Video Table
		var vidgeoQuery = new Parse.Query(vidgeoStore);
		// Limit Query
		vidgeoQuery.limit(1000);
		// Order by date
		vidgeoQuery.descending("createdAt");
	
		vidgeoQuery.find({
			//-------------------------------------------------------
			// Find Success
			success: function(videos) {
				for (var i = 0; i < videos.length; ++i) {
					var video = videos[i];
					(function(video) {
						// SQL Like
						dataQuery.equalTo("videoID", video.get("videoID"));
						// Retrieve Like Video
						dataQuery.find({
							success: function(vidgeos) {
								// The object was retrieved successfully.
								var vidgeo;
								if(vidgeos.length >= 1) {
									vidgeo = vidgeos[0];
									// If not previously set already
									vidgeo.set("thumbnail",video.get("videoData"));
									// Attach the video file to vidgeo-loc object
									vidgeo.save();
									//if(vidgeo.id === "6oBWtYnJN0") {
									//	status.success("Success; exiting cleanly.");
									//}
								}
							},
							error: function(object, error) {
								response.error("Currently Unable to fetch Vidgeo loc obj and set it's video");
							}
						});
					}(video));}
				},
				//-------------------------------------------------------
				// Find Error
				error: function() {
					response.error("Error with migration.");
				}
			});
		});

	// ---------------------------------------------------
	// END OF MIGRATION CODE------------------------------
	// ---------------------------------------------------
