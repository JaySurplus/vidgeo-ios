// ----- VIDGEO ACCESS METHOD -----
// --- parameters: userLocation
Parse.Cloud.define("vidgeosNearUser", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.limit(1000);
	query.near("location", request.params.userLocation);
	//add the "thumbnail" to the list, if so desired
	query.select("flagged", "location", "lockedVidgeo", "rateDown","rateUp","user","viewCount");
	query.find({
	success: function(vidgeos) {
	// The object was retrieved successfully.
		if(vidgeos){
			response.success(vidgeos);
		}
		return;
	},
	error: function(object, error) {
	response.error("Oops! Currently Unable to fetch Vidgeos");
	}
	});
});

// ----- FETCH VIDEO FILE  -----
// -- parameters: vidgeo (the vidgeo objectID)
Parse.Cloud.define("videoForVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.get(request.params.vidgeo, {
		success: function(vidgeo) {
			lock = vidgeo.get("lockedVidgeo")
			if(lock) {
				if(lock === request.params.passphrase){
					setViewCount(vidgeo);
					response.success(vidgeo);
				}else {
					response.error("Incorrect passphrase");
				}
			}else {
				setViewCount(vidgeo);
				response.success(vidgeo);
			}
			return;
		},
		error: function(object, error) {
			response.error("Uh oh, Unable to fetch the selected Vidgeo! Try again later.");
		}
	});
});

function setViewCount(vidgeo) {
	Parse.Cloud.useMasterKey();
    count = vidgeo.get("viewCount");
	count +=1;
	vidgeo.set("viewCount",count);
	vidgeo.save();
	return;
}

// ----- DELETE VIDGEO FILE  -----
// -- parameters: vidgeo (the vidgeo objectID)
Parse.Cloud.define("deleteVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.get(request.params.vidgeo, {
		success: function(vidgeo) {
			if(vidgeo.get("user").id === request.user.id){
				vidgeo.destroy({
				  success: function(myObject) {
					response.success("Deleted successfully");
				  },
				  error: function(myObject, error) {
					response.error("Delete was unsuccessful");
				  }
				});
			}else{
				response.error("Delete was unsuccessful");
			}
			return;
		},
		error: function(object, error) {
			response.error("Delete was unsuccessful");
		}
	});
});

// ----- FETCH THUMBNAIL FILE  -----
// -- parameters:  NULL
Parse.Cloud.define("thumbnailForUserVidgeos", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.limit(1000);
	query.descending("createdAt");
	query.equalTo("user", request.user);
	query.select("flagged", "location", "lockedVidgeo", "rateDown","rateUp","thumbnail","user","viewCount");
	query.find({
	success: function(vidgeos) {
	// The object was retrieved successfully.
		if(vidgeos) {
			response.success(vidgeos);
			return;
		} else {
			return;
		}
		
	},
	error: function(object, error) {
	response.error("Oops! Currently Unable to fetch Vidgeos");
	}
	});
});

// ----- FETCH THUMBNAIL FILE  -----
// -- parameters:  NULL
Parse.Cloud.define("thumbnailForUserFavorites", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.limit(1000);
	query.descending("createdAt");
	query.equalTo("rateUp", request.user.id);
	query.select("flagged", "location", "lockedVidgeo", "rateDown","rateUp","thumbnail","user","viewCount");
	query.find({
	success: function(vidgeos) {
	// The object was retrieved successfully.
		if(vidgeos){
			response.success(vidgeos);
		}
		return;
	},
	error: function(object, error) {
	response.error("Oops! Currently Unable to fetch Vidgeos");
	}
	});
});

// ----- RATING FUNCTIONS ---------

Parse.Cloud.define("rateUp", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.get(request.params.vidgeoID, {
	success: function(vidgeo) {
	// The object was retrieved successfully.
		var ratings = vidgeo.get("rateUp");
		 
		if(!ratings){
			ratings = [];
		}
		if(ratings.indexOf(request.params.userId) > -1){
		//if they have already rated it up
			response.error("User has already liked this vidgeo");
		}else{
			ratings.push(request.params.userId);
			vidgeo.set("rateUp", ratings);
			vidgeo.save(null, {
				success: function(vidgeo) {
					//have successfully updated the upVotes
					response.success("Rated vidgeo Up");
				}
			});
		}
		return;
	},
	error: function(object, error) {
		response.error("Currently Unable to upvote the selected Vidgeo");
	}
	});
});
Parse.Cloud.define("rateDown", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.get(request.params.vidgeoID, {
	success: function(vidgeo) {
	// The object was retrieved successfully.
		var ratings = vidgeo.get("rateDown");
		if(!ratings){
			ratings = [];
		}
		if(ratings.indexOf(request.params.userId) > -1){
		//if they have already rated it up
			response.error("User has already liked this vidgeo");
		}else{
			ratings.push(request.params.userId);
			vidgeo.set("rateDown", ratings);
			vidgeo.save(null, {
				success: function(vidgeo) {
					//have successfully updated the upVotes
					response.success("Rated vidgeo Down");
				}, error: function(vidgeo, error) {
				response.error("Currently Unable to down vote the selected Vidgeo: "+error);
			}
			});
		}
	},
	error: function(object, error) {
	// error is a Parse.Error with an error code and description.
	response.error("Currently Unable to upvote the selected Vidgeo");
	}
	});
});

// ----- LOCK FUNCTIONS ---------
//creates an entry in the VidgeoLock table for locking a video file
Parse.Cloud.define("lockUserVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.get(request.params.vidgeo, {
	success: function(vidgeo) {
	// The object was retrieved successfully.
		//only the creator can modify the password
		if(vidgeo.get("user").id === request.user.id){
			//we should update the new password
			vidgeo.set("lockedVidgeo",request.params.passphrase);
			vidgeo.save(null, {
				success: function(vidgeoLock) {
						//have successfully updated the the vidgeoLock
						response.success();
					},
					error: function(error) {
						response.error("Oops! Unable to lock the selected Vidgeo: ");
					}
			});	
		}
	},
	error: function(object, error) {
		response.error("Oops! Unable to lock the selected Vidgeo: ");
	}
	});
});


//need : Unlock Vidgeo
Parse.Cloud.define("unlockPublicVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.get(request.params.vidgeo, {
		success: function(vidgeoObj) {
		// The object was retrieved successfully.
			// compare the password
			if(vidgeoObj.get("lockedVidgeo") === request.params.passphrase){
				response.success(vidgeoObj);
			}else{
				response.error("Incorrect password ");
			}
		},
		error: function(object, error) {
			response.error("Oops! Unable to fetch the selected vidgeo Lock");
		}
	});
});

//need to give creator of vidgeo access without password
Parse.Cloud.define("fetchUserLockedVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.get(request.params.vidgeo, {
	success: function(vidgeoObj) {
	// The object was retrieved successfully.
		if(vidgeoObj.get("user").id === request.user.id){
			response.success(vidgeoObj);
		}else{
			response.error("Incorrect user permissions ");
		}
		
	},
	error: function(object, error) {
		response.error("Oops! Unable to fetch the selected vidgeo Lock ");
	}
	});
});

//need to let the creator unlock their vidgeo
Parse.Cloud.define("unlockUserVidgeo", function(request, response) {
	Parse.Cloud.useMasterKey();
		var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.get(request.params.vidgeo, {
		success: function(vidgeoObj) {
		// The object was retrieved successfully.
			if(vidgeoObj.get("user").id === request.user.id){
				vidgeoObj.unset("lockedVidgeo");
				vidgeoObj.save(null, {
						success: function(vidgeo) {
							//have successfully updated the vidgeoInfo
							//can now delete the vidgeoLock
							response.success();
						},
						error: function(videoLock, error) {
							response.error("Oops! Unable to remove vidgeo lock");
						}
					});
			}else{
				response.error("Incorrect user permissions ");
			}	
		},
		error: function(object, error) {
			response.error("Currently unable to fetch the selected vidgeo Lock: "+error);
		}
	});
});

Parse.Cloud.beforeSave("VidgeoStore", function(request, response) {
  if (!request.user.get("version")) {
    response.error("your iPhone version is not up-to-date. Please download a newer version");
  } else {
    response.success();
  }
});

Parse.Cloud.define("usernameForID", function(request, response) {
	Parse.Cloud.useMasterKey();
	var query = new Parse.Query(Parse.User);
	query.get(request.params.userId, {
		success: function(user) {
			//have successfully fetched the users videoData PFObject
			response.success(user);
		},
		error: function(user, error) {
			response.error("Currently unable to fetch the username : "+error);
		}
	});
});

// ----- VIDGEO WEB FUNCTIONS ---------
Parse.Cloud.define("topVidgeosWeb", function(request, response) {
	Parse.Cloud.useMasterKey();
	var dataStore = Parse.Object.extend("dataStore");
	var query = new Parse.Query(dataStore);
	query.limit(1000);
	query.notEqualTo("flagged", false);
	query.equalTo("lockedVidgeo", undefined);
	query.descending("viewCount");
	// 400 Error when i include select? commenting out for now
//	query.select("objectId","location","user","viewCount", "thumbnail", "createdAt");
	query.find({
		success: function(vidgeos) {
		    // The object was retrieved successfully.
			if(vidgeos) {
				response.success(vidgeos);
			}
			return;
		},
		error: function(object, error) {
			response.error("Oops! Currently Unable to fetch Vidgeos");
		}
	});
});